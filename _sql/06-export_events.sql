-- IMPORT SHOWS

TRUNCATE thewordradio_exports.events;

INSERT INTO thewordradio_exports.events(
  `id`,
  `created_at`,
  `updated_at`,
  `order`,
  `title`,
  `slug`,
  `intro`,
  `text`,
  `published`,
  `featured`
)

      SELECT
      articles.id,
      `created_at`,
      `updated_at`,
      `order`,
      article_translations.title,
      article_translations.slug,
      article_translations.intro,
      article_translations.text,
      `published`,
      0
      FROM `articles`
      LEFT JOIN article_translations
      ON article_translations.article_id = articles.id
      WHERE articles.parent_id = 1928
      GROUP BY articles.id;


-- Copy data

SET FOREIGN_KEY_CHECKS = 0;
SET GLOBAL FOREIGN_KEY_CHECKS=0;

INSERT INTO `thewordradio`.`events`(
    `id`,
    `created_at`,
    `updated_at`,
    `order`,
    `title`,
    `slug`,
    `intro`,
    `text`,
    `published`,
    `featured`
)
  SELECT
      `id`,
      `created_at`,
      `updated_at`,
      `order`,
      `title`,
      `slug`,
      `intro`,
      `text`,
      `published`,
      `featured`
  FROM
      `thewordradio_exports`.`events`



-- Match medias

UPDATE medias
INNER JOIN events ON (medias.mediatable_id = events.id)
SET medias.mediatable_type = 'App\\Event'
