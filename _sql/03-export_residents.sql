-- IMPORT WORD RESIDENTS

-- 1. Duplicate the residents table, then..

TRUNCATE thewordradio_exports.residents;

INSERT INTO thewordradio_exports.residents(
  `id`,
  `created_at`,
  `updated_at`,
  `order`,
  `title`,
  `slug`,
  `intro`,
  `text`,
  `published`,
  `parent_id`,
  `featured`
)

      SELECT
      articles.id,
      `created_at`,
      `updated_at`,
      `order`,
      article_translations.title,
      article_translations.slug,
      CONCAT_WS(
        ' \n\n',
        NULLIF(link, ''),
        NULLIF(facebook, ''),
        NULLIF(twitter, ''),
        NULLIF(instagram, ''),
        NULLIF(soundcloud, ''),
        NULLIF(mixcloud, ''),
        NULLIF(bandcamp, '')
      )
      AS intro,
      article_translations.text,
      `published`,
      `parent_id`,
      0

      FROM `articles`
      LEFT JOIN article_translations
      ON article_translations.article_id = articles.id
      WHERE articles.parent_id = 2
      GROUP BY articles.id;



-- Add residents taxonomy
-- Check ID related to current toaxonomy

INSERT INTO taxonomyables (taxonomyable_id, taxonomy_id, taxonomyable_type)
  SELECT id, 682, 'App\\Resident'
  FROM residents
  WHERE parent_id = 2


-- IMPORT SPECIALS RESIDENTS

INSERT INTO thewordradio_exports.residents(
  `id`,
  `created_at`,
  `updated_at`,
  `order`,
  `title`,
  `slug`,
  `intro`,
  `text`,
  `published`,
  `parent_id`,
  `featured`
)

      SELECT
      articles.id,
      `created_at`,
      `updated_at`,
      `order`,
      article_translations.title,
      article_translations.slug,
      CONCAT_WS(
        ' \n\n',
        NULLIF(link, ''),
        NULLIF(facebook, ''),
        NULLIF(twitter, ''),
        NULLIF(instagram, ''),
        NULLIF(soundcloud, ''),
        NULLIF(mixcloud, ''),
        NULLIF(bandcamp, '')
      )
      AS intro,
      article_translations.text,
      `published`,
      `parent_id`,
      0

      FROM `articles`
      LEFT JOIN article_translations
      ON article_translations.article_id = articles.id
      WHERE articles.parent_id = 689
      GROUP BY articles.id;

-- The specials taxonomy
INSERT INTO taxonomyables (taxonomyable_id, taxonomy_id, taxonomyable_type)
  SELECT id, 684, 'App\\Resident' FROM residents
  WHERE parent_id = 689



-- Match resident medias

UPDATE medias
INNER JOIN residents ON (medias.mediatable_id = residents.id)
SET medias.mediatable_type = 'App\\Resident'


-- Copy all resident to actual DB

INSERT INTO `thewordradio`.`residents`
  (`id`, `created_at`, `updated_at`, `order`, `title`, `slug`, `intro`, `text`, `published`, `featured`)
  SELECT `id`, `created_at`, `updated_at`, `order`, `title`, `slug`, `intro`, `text`, `published`, `featured`
  FROM `thewordradio_exports`.`residents`
