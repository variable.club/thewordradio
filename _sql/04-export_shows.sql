-- IMPORT SHOWS

TRUNCATE thewordradio_exports.shows;

INSERT INTO thewordradio_exports.shows(
  `id`,
  `created_at`,
  `updated_at`,
  `order`,
  `title`,
  `slug`,
  `sound_url`,
  `intro`,
  `text`,
  `tracklist`,
  `published`,
  `featured`,
  `start_date`,
  `resident_id`
)

      SELECT
      articles.id,
      `created_at`,
      `updated_at`,
      `order`,
      article_translations.title,
      article_translations.slug,
      `mixcloud_show_url`,
      article_translations.intro,
      article_translations.text,
      `tracklist`,
      `published`,
      0,
      `date`,
      `related_article_id`
      FROM `articles`
      LEFT JOIN article_translations
      ON article_translations.article_id = articles.id
      WHERE articles.parent_id = 1
      GROUP BY articles.id;


-- Copy data

SET FOREIGN_KEY_CHECKS = 0;
SET GLOBAL FOREIGN_KEY_CHECKS=0;

INSERT INTO `thewordradio`.`shows`(
    `id`,
    `created_at`,
    `updated_at`,
    `order`,
    `title`,
    `slug`,
    `sound_url`,
    `intro`,
    `text`,
    `tracklist`,
    `published`,
    `featured`,
    `start_date`,
    `resident_id`
)
  SELECT
      `id`,
      `created_at`,
      `updated_at`,
      `order`,
      `title`,
      `slug`,
      `sound_url`,
      `intro`,
      `text`,
      `tracklist`,
      `published`,
      `featured`,
      `start_date`,
      `resident_id`
  FROM
      `thewordradio_exports`.`shows`



-- Match medias

UPDATE thewordradio_exports.medias
INNER JOIN thewordradio_exports.shows ON (medias.mediatable_id = shows.id)
SET medias.mediatable_type = 'App\\Show'
