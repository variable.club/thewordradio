-- GET ALL GUEST SHOW AND COPY TO RESIDENTS

INSERT INTO thewordradio_exports.residents(
  `created_at`,
  `updated_at`,
  `order`,
  `title`,
  `slug`,
  `intro`,
  `text`,
  `published`,
  `featured`,
  `parent_id`
)

      SELECT
      `created_at`,
      `updated_at`,
      `order`,
      article_translations.title,
      article_translations.slug,
      article_translations.intro,
      article_translations.text,
      `published`,
      `parent_id`,
      0
      FROM `articles`
      LEFT JOIN article_translations
      ON article_translations.article_id = articles.id
      WHERE articles.related_article_id = 4
      GROUP BY articles.id;


-- Copy data

SET FOREIGN_KEY_CHECKS = 0;
SET GLOBAL FOREIGN_KEY_CHECKS=0;

INSERT INTO `thewordradio`.`residents`(
  `id`,
  `created_at`,
  `updated_at`,
  `order`,
  `title`,
  `slug`,
  `intro`,
  `text`,
  `published`,
  `featured`
)
  SELECT
      `id`,
      `created_at`,
      `updated_at`,
      `order`,
      `title`,
      `slug`,
      `intro`,
      `text`,
      `published`,
      `featured`
  FROM
      `thewordradio_exports`.`residents`
  WHERE `thewordradio_exports`.`residents`.`parent_id` = 0


  -- Add residents taxonomy
  -- Check ID related to current toaxonomy

  INSERT INTO taxonomyables (taxonomyable_id, taxonomy_id, taxonomyable_type)
    SELECT id, 683, 'App\\Resident'
    FROM residents
    WHERE parent_id = 0
