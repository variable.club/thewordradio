-- Create a temporary table containing all the articles

CREATE TABLE full_articles

    SELECT
    articles.id,
    article_translations.title,
    article_translations.slug,
    article_translations.intro,
    article_translations.text,
    `created_at`,
    `updated_at`,
    `order`,
    `published`,
    `parent_id`,
    `image_une`,
    `date`,
    `tracklist`,
    `mixcloud_show_url`,
    `link`,
    `facebook`,
    `twitter`,
    `instagram`,
    `related_article_id`,
    `start_hour`,
    `end_hour`,
    `mp3_file`,
    `recommended`,
    `soundcloud`,
    `mixcloud`,
    `bandcamp`

    FROM `articles`
    LEFT JOIN article_translations
    ON article_translations.article_id = articles.id
    GROUP BY articles.id
