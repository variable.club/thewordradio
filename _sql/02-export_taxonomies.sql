-- IMPORT TAXONOMIES

TRUNCATE thewordradio_exports.taxonomies;

INSERT INTO thewordradio_exports.taxonomies(
  `id`,
  `parent_id`,
  `name`,
  `slug`,
  `order`,
  `created_at`,
  `updated_at`
)

    SELECT
      tags.id,
      `parent_id`,
      tag_translations.name,
      tag_translations.slug,
      `order`,
      `created_at`,
      `updated_at`
    FROM `tags`
    LEFT JOIN `tag_translations`
    ON tag_translations.tag_id = tags.id
    GROUP BY tags.id;


-- LINK TAXONOMIES 2 SHOWS

TRUNCATE thewordradio_exports.taxonomyables;

INSERT INTO thewordradio_exports.taxonomyables (
  taxonomy_id,
  taxonomyable_id,
  taxonomyable_type
)
  SELECT
    tag_id,
    article_id,
    'App\\Show'
  FROM thewordradio_v1.article_tag
