-- IMPORT ALL MEDIAS

INSERT INTO thewordradio_exports.medias(
  `id`,
  `mediatable_type`,
  `mediatable_id`,
  `collection_name`,
  `name`,
  `description`,
  `file_name`,
  `mime_type`,
  `custom_properties`,
  `order`,
  `width`,
  `height`,
  `created_at`,
  `updated_at`
)

      SELECT
      `id`,
      `mediatable_type`,
      `mediatable_id`,
      `type`,
      `alt`,
      `description`,
      `name`,
      `ext`,
      '',
      `order`,
      `width`,
      `height`,
      `created_at`,
      `updated_at`

      FROM `medias`
      WHERE `mediatable_type` IS NOT NULL


-- Rebuild mime type
UPDATE `medias` SET
`mime_type` = 'image/jpeg'
WHERE `mime_type` = 'jpg'

UPDATE `medias` SET
`mime_type` = 'image/jpeg'
WHERE `mime_type` = 'jpeg'

UPDATE `medias` SET
`mime_type` = 'image/png'
WHERE `mime_type` = 'png'

UPDATE `medias` SET
`mime_type` = 'image/gif'
WHERE `mime_type` = 'gif'
