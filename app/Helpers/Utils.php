<?php
use Carbon\Carbon;

if (! function_exists('formatedDate')) {
    function formatedDate($date, $hourFormat=1){
      if(!empty($date)){
        $date = ($hourFormat)
          ? Carbon::createFromFormat('Y-m-d H:i:s', $date)
          : Carbon::createFromFormat('Y-m-d', $date);
          return ucfirst($date->format('d.m.y'));
      }else{
        return null;
      }
    }
}


/**
* Build navigation from Admin routes
*
* @return string
*/

if (! function_exists('buildAdminNav')) {
  function buildAdminNav(){
    $i = 0;
    foreach (\Route::getRoutes()->getIterator() as $route){
      if(
        strpos($route->getName(), 'admin') !== false &&
        strpos($route->getName(), 'index') !== false &&
        $route->getName() != 'admin.users.index' &&
        $route->getName() != 'admin.roles.index' &&
        $route->getName() != 'admin.permissions.index' &&
        $route->getName() != 'admin.settings.index' &&
        $route->getName() != 'admin.taxonomies.index' &&
        $route->getName() != 'admin.medias.index'
      ){
        $route_name = $route->getName();
        $title = explode('.',$route_name);
        $routes[$i]['name'] = $route_name;
        $routes[$i]['title'] = $title[1];
        $i++;
      }
    }
    return $routes;
  }
}



/**
* Build buildMixcloudUrl
*
* @return string
*/

if (! function_exists('buildMixcloudUrl')) {
  function buildMixcloudUrl($url){
    $parts = explode('/', $url);
    $clean_url = '';
    foreach($parts as $key => $el) {
      if(
        empty($el)
        or strpos($el, 'http')!== false
        or strpos($el, 'upload')!== false
        or strpos($el, 'mixcloud')!== false
        or strpos($el, 'complete')!== false
      ):
        unset($parts[$key]);
      else:
        $clean_url .= $parts[$key].'/';
      endif;
    }
    return $clean_url;
  }
}



////
// [Search] Convert html > ul > li to a PHP array
////

if (! function_exists('ul_to_array')) {
  function ul_to_array ($ul) {
    if (is_string($ul)) {
      // encode ampersand appropiately to avoid parsing warnings
      $ul = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $ul);
      if (!$ul = simplexml_load_string($ul, "SimpleXMLElement", LIBXML_NOERROR)) {
        // trigger_error("Syntax error in UL/LI structure");
        return FALSE;
      }
      return ul_to_array($ul);
    } else if (is_object($ul)) {
      $output = array();
      foreach ($ul->li as $li) {
        $output[] = (isset($li->ul)) ? ul_to_array($li->ul) : (string) $li;
      }
      return $output;
    } else return FALSE;
  }
}


////
// [Search] Search for a string in array
////

if (! function_exists('li_search')) {
  function li_search ($array, $keywords) {
    $output = '';
    foreach ($array as $el) {
      if (strpos(strtolower($el), strtolower($keywords)) !== false) {
        $output = $el;
        break;
      }
    }
    return $output;
  }
}
