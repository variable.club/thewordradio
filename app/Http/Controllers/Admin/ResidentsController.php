<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\Admin\ArticleRequest;
use App\Media;
use App\Resident;
use App\Taxonomy;
use Carbon\Carbon;
use DB;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Lang;
use Validator;


class residentsController extends AdminController
{

  public function __construct(){
    $this->table_type = 'residents';
    Lang::setLocale(config('app.locale'));
    $this->middleware(['auth', 'permissions'])->except('index');
    // Define an array of all related taxonomie's parents
    $this->taxonomies = [679];
    parent::__construct();
  }

  /**
   * List all articles
   *
   * @param  string  $parent_id
   * @return \Illuminate\Http\Response
   */

  public function index(){
    $articles = Resident::orderBy('title')
                  ->get();
    $data = array(
      'page_class' => 'index',
      'page_title' => 'residents',
      'page_id'    => 'index-residents',
      'table_type' => $this->table_type,
    );
    return view('admin/templates/residents-index', compact('articles', 'data'));
  }


  /**
   * Get articles for datatables (ajax)
   *
   * @return \Illuminate\Http\Response
   */

  public function getDataTable(){
    return \DataTables::of(Resident::orderBy('title')->get())
                        ->addColumn('title_link', function ($article) {
                          return '<div class="text-content"><a href="' . route('admin.residents.edit', $article->id) . '" class="text-content">' . $article->title . '</a></div>';
                        })
                        ->addColumn('action', function ($article) {
                          return '<a href="' . route('admin.residents.edit', $article->id) . '" class="link">' . __('admin.edit') . '</a>';
                        })
                        ->addColumn('category', function ($article) {
                          if(!empty($article->residentCategory->first())):
                            $category = $article->residentCategory->first();
                            return $category->name;
                          else:
                            return '';
                          endif;
                        })
                        ->rawColumns(['title_link', 'action'])
                        ->make(true);
  }


  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function edit($id){
    $article = Resident::findOrFail($id);
    $data = array(
      'page_class' => 'residents',
      'page_title' => 'residents edit',
      'page_id'    => 'edit-residents',
      'table_type' => $this->table_type,
    );
    // dd($article->categories);
  	return view('admin/templates/resident-edit',  compact('article', 'data'));
  }


  /**
   * Show the form for creating a new resource.
   *
   * @param  string  $parent_slug
   * @return \Illuminate\Http\Response
   */

  public function create(){
    $data = array(
      'page_class' => 'Resident create',
      'page_title' => 'resident create',
      'page_id'    => 'create-resident',
      'table_type' => $this->table_type,
    );
    $article = new Resident;
    return view('admin.templates.resident-edit', compact('article', 'data'));
  }


  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   */

  public function update(Resident $resident, ArticleRequest $request){
    $request['featured'] = ($request['featured']) ? 1 : 0;
    // Save article
    return $this->saveObject($resident, $request, $this->taxonomies);
  }


  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */

  public function store(ArticleRequest $request){
    // Create article
    return $this->createObject(Resident::class, $request, 'redirect', $this->taxonomies);
  }


  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function destroy(Resident $resident){
    return $this->destroyObject($resident);
  }


  /**
   * Reorder the articles relative to parent
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return Json response
   */

   public function reorder(Request $request){
     return $this->orderObject(Resident::class, $request);
   }
}
