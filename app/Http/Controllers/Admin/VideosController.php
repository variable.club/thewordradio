<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\Admin\ArticleRequest;
use App\Media;
use App\Taxonomy;
use App\Video;
use Carbon\Carbon;
use DB;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Lang;
use Validator;


class videosController extends AdminController
{


    public function __construct(){
      $this->table_type = 'videos';
      Lang::setLocale(config('app.locale'));
      $this->middleware(['auth', 'permissions'])->except('index');
      // Define an array of all related taxonomie's parents
      $this->taxonomies = [1,681];
      parent::__construct();
    }

  /**
   * List all articles
   *
   * @param  string  $parent_id
   * @return \Illuminate\Http\Response
   */

   public function index($parent_id = 0){
     $articles = Video::where('parent_id', $parent_id)
                   ->orderBy('order', 'asc')
                   ->get();
     if($parent_id != 0){
       $parent_article = Video::findOrFail($parent_id);
     }else{
       $parent_article = null;
     }
     $data = array(
       'page_class' => 'index',
       'page_title' => 'videos',
       'page_id'    => 'index-videos',
       'table_type' => $this->table_type,
     );
     return view('admin/templates/videos-index', compact('articles', 'parent_article', 'data'));
   }


  /**
   * Get articles for datatables (ajax)
   *
   * @return \Illuminate\Http\Response
   */

  public function getDataTable($parent_id = 0){
    return \DataTables::of(Video::where('parent_id', $parent_id)
                        ->orderBy('order', 'asc')
                        ->get())
                        ->addColumn('action', function ($article) {
                          if(count($article->children)) {
                            return '<a href="' . route('admin.videos.index', $article->id) . '" class="link">' . __('admin.view_subpage') . '</a> <a href="' . route('admin.videos.edit', $article->id) . '" class="link">' . __('admin.edit') . '</a>';
                          }elseif($article->parent_id == 0){
                            return '<a href="' . route('admin.videos.create', $article->id) . '" class="link">' . __('admin.add_subpage') . '</a> <a href="' . route('admin.videos.edit', $article->id) . '" class="link">' . __('admin.edit') . '</a>';
                          }else {
                            return '<a href="' . route('admin.videos.edit', $article->id) . '" class="link">' . __('admin.edit') . '</a>';
                          }
                        })
                        ->setRowClass(function ($article) {
                          if(count($article->children)) {
                           return 'has-child';
                          }
                        })
                        ->make(true);
  }


  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function edit($id){
    $article = Video::findOrFail($id);
    $data = array(
      'page_class' => 'videos',
      'page_title' => 'videos edit',
      'page_id'    => 'edit-videos',
      'table_type' => $this->table_type,
    );
    // dd($article->categories);
  	return view('admin/templates/video-edit',  compact('article', 'data'));
  }


  /**
   * Show the form for creating a new resource.
   *
   * @param  string  $parent_slug
   * @return \Illuminate\Http\Response
   */

  public function create($parent_id = 0){
    $data = array(
      'page_class' => 'Video create',
      'page_title' => 'videos create',
      'page_id'    => 'create-videos',
      'table_type' => $this->table_type,
    );
    $article = new Video;
    $article->parent_id = $parent_id;
    $article->parent = Video::where('id', $parent_id)->first();
    return view('admin.templates.video-edit', compact('article', 'data'));
  }


  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   */

  public function update(Video $video, ArticleRequest $request){
    $request['featured'] = ($request['featured']) ? 1 : 0;
    // Save article
    return $this->saveObject($video, $request, $this->taxonomies);
  }


  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */

  public function store(ArticleRequest $request){
    // Create article
    return $this->createObject(Video::class, $request, 'redirect', $this->taxonomies);
  }


  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function destroy(Video $video){
    return $this->destroyObject($video);
  }


  /**
   * Reorder the articles relative to parent
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return Json response
   */

   public function reorder(Request $request){
     return $this->orderObject(Video::class, $request);
   }
}
