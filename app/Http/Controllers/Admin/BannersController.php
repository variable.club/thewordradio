<?php

namespace App\Http\Controllers\Admin;
use App\Banner;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\Admin\ArticleRequest;
use App\Media;
use App\Taxonomy;
use Carbon\Carbon;
use DB;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Lang;
use Validator;


class bannersController extends AdminController
{

  public function __construct(){
    $this->table_type = 'banners';
    Lang::setLocale(config('app.locale'));
    $this->middleware(['auth', 'permissions'])->except('index');
    // Define an array of all related taxonomie's parents
    $this->taxonomies = [];
    parent::__construct();
  }

  /**
   * List all articles
   *
   * @param  string  $parent_id
   * @return \Illuminate\Http\Response
   */

  public function index(){
    $articles = Banner::orderBy('start_date', 'desc')
                  ->get();
    $data = array(
      'page_class' => 'index',
      'page_title' => 'banners',
      'page_id'    => 'index-banners',
      'table_type' => $this->table_type,
    );
    return view('admin/templates/banners-index', compact('articles', 'data'));
  }


  /**
   * Get articles for datatables (ajax)
   *
   * @return \Illuminate\Http\Response
   */

  public function getDataTable(){
    return \DataTables::of(Banner::orderBy('start_date', 'desc')->get())
                        ->addColumn('title_link', function ($article) {
                          return '<div class="text-content"><a href="' . route('admin.banners.edit', $article->id) . '" class="text-content">' . $article->title . '</a></div>';
                        })
                        ->addColumn('dates', function ($article) {
                          $start_date = $end_date = null;
                          if(!empty($article->start_date)):
                            $start_date =  date('d.m.y',strtotime($article->start_date));
                          endif;
                          if(!empty($article->end_date)):
                            $end_date =  ' > '.date('d.m.y',strtotime($article->end_date));
                          endif;
                          return $start_date.$end_date;
                        })
                        ->addColumn('action', function ($article) {
                          return '<a href="' . route('admin.banners.edit', $article->id) . '" class="link">' . __('admin.edit') . '</a>';
                        })
                        ->rawColumns(['title_link', 'action'])
                        ->make(true);
  }


  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function edit($id){
    $article = Banner::findOrFail($id);
    $data = array(
      'page_class' => 'banners',
      'page_title' => 'banners edit',
      'page_id'    => 'edit-banners',
      'table_type' => $this->table_type,
    );
    // dd($article->categories);
  	return view('admin/templates/banner-edit',  compact('article', 'data'));
  }


  /**
   * Show the form for creating a new resource.
   *
   * @param  string  $parent_slug
   * @return \Illuminate\Http\Response
   */

  public function create(){
    $data = array(
      'page_class' => 'Banner create',
      'page_title' => 'banners create',
      'page_id'    => 'create-banners',
      'table_type' => $this->table_type,
    );
    $article = new Banner;
    return view('admin.templates.banner-edit', compact('article', 'data'));
  }


  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   */

  public function update(Banner $banner, ArticleRequest $request){
    // Save article
    return $this->saveObject($banner, $request, $this->taxonomies);
  }


  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */

  public function store(ArticleRequest $request){
    // Create article
    return $this->createObject(Banner::class, $request, 'redirect', $this->taxonomies);
  }


  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function destroy(Banner $banner){
    return $this->destroyObject($banner);
  }


  /**
   * Reorder the articles relative to parent
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return Json response
   */

   public function reorder(Request $request){
     return $this->orderObject(Banner::class, $request);
   }
}
