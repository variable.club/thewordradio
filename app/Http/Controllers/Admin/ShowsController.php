<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\Admin\ArticleRequest;
use App\Media;
use App\Show;
use App\Taxonomy;
use App\Resident;
use Carbon\Carbon;
use DB;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Lang;
use Validator;


class showsController extends AdminController
{

  public function __construct(){
    $this->table_type = 'shows';
    Lang::setLocale(config('app.locale'));
    $this->middleware(['auth', 'permissions'])->except('index');
    // Define an array of all related taxonomie's parents
    $this->taxonomies = [1];
    parent::__construct();
  }

  /**
   * List all articles
   *
   * @param  string  $parent_id
   * @return \Illuminate\Http\Response
   */

  public function index(){
    $articles = Show::orderBy('start_date', 'desc')
                  ->get();
    $data = array(
      'page_class' => 'index',
      'page_title' => 'shows',
      'page_id'    => 'index-shows',
      'table_type' => $this->table_type,
    );
    return view('admin/templates/shows-index', compact('articles', 'data'));
  }


  /**
   * Get articles for datatables (ajax)
   *
   * @return \Illuminate\Http\Response
   */

  public function getDataTable(){
    return \DataTables::of(Show::orderBy('start_date', 'desc')->get())
                        ->addColumn('title_link', function ($article) {
                          return '<div class="text-content"><a href="' . route('admin.shows.edit', $article->id) . '" class="text-content">' . $article->title . '</a></div>';
                        })
                        ->addColumn('action', function ($article) {
                          return '<a href="' . route('admin.shows.edit', $article->id) . '" class="link">' . __('admin.edit') . '</a>';
                        })
                        ->addColumn('start_date', function ($article) {
                          if($article->start_date):
                            return date('d.m.y',strtotime($article->start_date));
                          else:
                            return '';
                          endif;
                        })
                        ->addColumn('resident', function ($article) {
                          if(!empty($article->resident)):
                            $resident = $article->resident;
                            return $resident->title;
                          else:
                            return '';
                          endif;
                        })
                        ->rawColumns(['title_link', 'action'])
                        ->make(true);
  }


  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function edit($id){
    $article = Show::findOrFail($id);
    $data = array(
      'page_class' => 'shows',
      'page_title' => 'shows edit',
      'page_id'    => 'edit-shows',
      'table_type' => $this->table_type,
    );
    // dd($article->categories);
  	return view('admin/templates/show-edit',  compact('article', 'data'));
  }


  /**
   * Show the form for creating a new resource.
   *
   * @param  string  $parent_slug
   * @return \Illuminate\Http\Response
   */

  public function create(){
    $data = array(
      'page_class' => 'Show create',
      'page_title' => 'show create',
      'page_id'    => 'create-shows',
      'table_type' => $this->table_type,
    );
    $article = new Show;
    return view('admin.templates.show-edit', compact('article', 'data'));
  }


  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   */

  public function update(Show $show, ArticleRequest $request){
    $request['featured'] = ($request['featured']) ? 1 : 0;
    // Attache Resident
    if(!empty($request->resident)):
      $resident = Resident::find($request->resident);
      // dd($resident);
      $show->resident()->associate($resident);
      $show->save();
    else:
      $show->resident()->dissociate();
      $show->save();
    endif;
    // Save article
    return $this->saveObject($show, $request, $this->taxonomies);
  }


  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */

  public function store(ArticleRequest $request){
    // Create article
    return $this->createObject(Show::class, $request, 'redirect', $this->taxonomies);
  }


  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function destroy(Show $show){
    return $this->destroyObject($show);
  }


  /**
   * Reorder the articles relative to parent
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return Json response
   */

   public function reorder(Request $request){
     return $this->orderObject(Show::class, $request);
   }
}
