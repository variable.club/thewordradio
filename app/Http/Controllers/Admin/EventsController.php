<?php

namespace App\Http\Controllers\Admin;
use App\Event;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\Admin\ArticleRequest;
use App\Media;
use App\Taxonomy;
use Carbon\Carbon;
use DB;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Lang;
use Validator;


class eventsController extends AdminController
{

  public function __construct(){
    $this->table_type = 'events';
    Lang::setLocale(config('app.locale'));
    $this->middleware(['auth', 'permissions'])->except('index');
    // Define an array of all related taxonomie's parents
    $this->taxonomies = [680];
    parent::__construct();
  }

  /**
   * List all articles
   *
   * @param  string  $parent_id
   * @return \Illuminate\Http\Response
   */

  public function index($parent_id = 0){
    $articles = Event::where('parent_id', $parent_id)
                  ->orderBy('order', 'asc')
                  ->get();
    if($parent_id != 0){
      $parent_article = Event::findOrFail($parent_id);
    }else{
      $parent_article = null;
    }
    $data = array(
      'page_class' => 'index',
      'page_title' => 'events',
      'page_id'    => 'index-events',
      'table_type' => $this->table_type,
    );
    return view('admin/templates/events-index', compact('articles', 'parent_article', 'data'));
  }


  /**
   * Get articles for datatables (ajax)
   *
   * @return \Illuminate\Http\Response
   */

  public function getDataTable($parent_id = 0){
    return \DataTables::of(Event::where('parent_id', $parent_id)
                        ->orderBy('order', 'asc')
                        ->get())
                        ->addColumn('action', function ($article) {
                          if(count($article->children)) {
                            return '<a href="' . route('admin.events.index', $article->id) . '" class="link">' . __('admin.view_subpage') . '</a> <a href="' . route('admin.events.edit', $article->id) . '" class="link">' . __('admin.edit') . '</a>';
                          }elseif($article->parent_id == 0){
                            return '<a href="' . route('admin.events.create', $article->id) . '" class="link">' . __('admin.add_subpage') . '</a> <a href="' . route('admin.events.edit', $article->id) . '" class="link">' . __('admin.edit') . '</a>';
                          }else {
                            return '<a href="' . route('admin.events.edit', $article->id) . '" class="link">' . __('admin.edit') . '</a>';
                          }
                        })
                        ->addColumn('dates', function ($article) {
                          $start_date = $end_date = null;
                          if(!empty($article->start_date)):
                            $start_date =  date('d.m.y',strtotime($article->start_date));
                          endif;
                          if(!empty($article->end_date)):
                            $end_date =  ' > '.date('d.m.y',strtotime($article->end_date));
                          endif;
                          return $start_date.$end_date;
                        })
                        ->setRowClass(function ($article) {
                          if(count($article->children)) {
                           return 'has-child';
                          }
                        })
                        ->make(true);
  }


  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function edit($id){
    $article = Event::findOrFail($id);
    $data = array(
      'page_class' => 'events',
      'page_title' => 'events edit',
      'page_id'    => 'edit-events',
      'table_type' => $this->table_type,
    );
    // dd($article->categories);
  	return view('admin/templates/event-edit',  compact('article', 'data'));
  }


  /**
   * Show the form for creating a new resource.
   *
   * @param  string  $parent_slug
   * @return \Illuminate\Http\Response
   */

  public function create($parent_id = 0){
    $data = array(
      'page_class' => 'Event create',
      'page_title' => 'events create',
      'page_id'    => 'create-events',
      'table_type' => $this->table_type,
    );
    $article = new Event;
    $article->parent_id = $parent_id;
    return view('admin.templates.event-edit', compact('article', 'data'));
  }


  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   */

  public function update(Event $event, ArticleRequest $request){
    $request['featured'] = ($request['featured']) ? 1 : 0;
    // Save article
    return $this->saveObject($event, $request, $this->taxonomies);
  }


  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */

  public function store(ArticleRequest $request){
    // Create article
    return $this->createObject(Event::class, $request, 'redirect', $this->taxonomies);
  }


  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function destroy(Event $event){
    return $this->destroyObject($event);
  }


  /**
   * Reorder the articles relative to parent
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return Json response
   */

   public function reorder(Request $request){
     return $this->orderObject(Event::class, $request);
   }
}
