<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Setting;
use App\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class eventsController extends FrontController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
      $this->minutes = 48*60; // 48h
      parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $events = Cache::remember('events-index', $this->minutes, function(){
        return $events = Event::where('published', 1)
                              ->where('parent_id', 0)
                              ->orderBy('start_date', 'desc')
                              ->get();
        });

      $data = array(
        'page_class' => 'wrapper wrapper--index',
        'page_title' => 'Agenda | The Word Radio',
        'page_description' => 'The Word Radio agenda',
        'page_name' => 'events.index',
      );
      return view('templates/index-events', compact('events', 'data'));
    }


    /**
     * Show the article
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($slug){
      $event = Cache::remember('event'.' '.$slug, $this->minutes, function() use($slug){
        return $event = event::where('slug', $slug)->first();
      });
      if($event):
        $data = array(
          'page_class' => 'event'.' '.$event->slug,
          'page_title' => $event->title. ' | The Word Radio',
          'page_description' => str_limit($event->text, 300),
          'page_name' => 'event.show',
        );
        return view('templates/event', compact('event', 'data'));
      else:
        abort(404);
      endif;
    }


    /**
     * Show the article (no childs events)
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function showSingle($slug){
      $event = Cache::remember('event'.' '.$slug, $this->minutes, function() use($slug){
        return $event = event::where('slug', $slug)->first();
      });
      if($event):
        $data = array(
          'page_class' => 'event'.' '.$event->slug,
          'page_title' => $event->title. ' | The Word Radio',
          'page_description' => str_limit($event->text, 300),
          'page_name' => 'event.show',
        );
        return view('templates/event-single', compact('event', 'data'));
      else:
        abort(404);
      endif;
    }
}
