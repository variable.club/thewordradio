<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Article;
use App\Setting;
use Illuminate\Support\Facades\Cache;

class ArticlesController extends FrontController{


  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct(){
    $this->minutes = 48*60; // 48h
    parent::__construct();
  }


  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $articles = Cache::remember('articles-index', $this->minutes, function(){
      return $articles = Article::where('published', 1)
                            ->where('parent_id', 0)
                            ->orderBy('order', 'asc')
                            ->orderBy('created_at', 'desc')
                            ->get();
      });

    $data = array(
      'page_class' => 'wrapper wrapper--index',
      'page_title' => 'Interviews | The Word Radio',
      'page_description' => 'The Word Radio interviews',
      'page_name' => 'articles.index',
    );
    return view('templates/index-articles', compact('articles', 'data'));
  }


  /**
   * Show the article
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function show($slug){

    $article = Cache::remember('article'.' '.$slug, $this->minutes, function() use($slug){
      return $article = Article::where('slug', $slug)->first();
    });
    if($article):
      $data = array(
        'page_class' => 'article'.' '.$article->slug,
        'page_title' => $article->title.' | The Word Radio',
        'page_description' => str_limit($article->text, 300),
        'page_name' => 'articles.show',
      );
      return view('templates/article', compact('article', 'data'));
    else:
      abort(404);
    endif;

  }

}
