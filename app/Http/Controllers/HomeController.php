<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Show;
use App\Event;
use App\Resident;
use App\Video;
use App\Article;
use App\Media;
use App\Setting;
use Illuminate\Support\Facades\Cache;

class HomeController extends FrontController{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
      $this->minutes = 48*60; // 48h
      parent::__construct();
    }


    /**
     * Show the homepage
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
      $settings = Setting::find([1,2]);
      $website_description = $settings[0]->content;
      $data = array(
        'page_class' => 'wrapper',
        'page_title' => 'The Word Radio',
        'page_description' => $website_description,
        'page_name' => 'home',

      );
      $featured_articles = Cache::remember('featured_articles', $this->minutes, function(){
        return $featured_articles = Article::where('published', 1)
                          ->where('featured', 1)
                          ->orderBy('created_at', 'desc')
                          ->get();
      });
      $featured_shows = Cache::remember('featured_shows', $this->minutes, function(){
        return $featured_shows = Show::where('published', 1)
                          ->where('featured', 1)
                          ->orderBy('created_at', 'desc')
                          ->get();
      });
      $featured_residents = Cache::remember('featured_residents', $this->minutes, function(){
        return $featured_residents = Resident::where('published', 1)
                          ->orderBy('created_at', 'desc')
                          ->where('featured', 1)
                          ->get();
      });
      $featured_events = Cache::remember('featured_events', $this->minutes, function(){
        return $featured_events = Event::where('published', 1)
                          ->where('featured', 1)
                          ->orderBy('start_date', 'desc')
                          ->get();
      });
      $featured_videos = Cache::remember('featured_videos', $this->minutes, function(){
        return $featured_videos = Video::where('published', 1)
                          ->where('featured', 1)
                          ->orderBy('created_at', 'desc')
                          ->get();
      });
      $latest_shows = Cache::remember('latest_shows', $this->minutes, function(){
        return $latest_shows = Show::where('published', 1)
                              ->orderBy('start_date', 'desc')
                              ->limit(12)
                              ->get();
      });
      return response()
            ->view('templates/home', compact('featured_articles','featured_shows','featured_residents','featured_shows','featured_events','featured_videos', 'latest_shows','data'));
    }

    /**
     * Show the schedule
     *
     * @return \Illuminate\Http\Response
     */

    public function schedule(){
      $data = array(
        'page_class' => 'wrapper',
        'page_title' => 'Schedule | The Word Radio',
        'page_description' => 'The Word Radio schedule',
        'page_name' => 'schedule',

      );
      return response()->view('templates/schedule', compact('data'));
    }
}
