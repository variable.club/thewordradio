<?php

namespace App\Http\Controllers;
use App;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Setting;
use App\Resident;
use App\Taxonomy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class residentsController extends FrontController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
      $this->minutes = 48*60; // 48h
      parent::__construct();
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($category = 'the-word')
    {
      $category = Taxonomy::where('slug', $category)->first();
      if(!empty($category)):
        $residents = Cache::remember('residents-index-'.$category->slug, $this->minutes, function() use($category){
          return $residents = $category->residentsByTaxonomy;
        });
        $category_name = ($category->slug == 'the-word') ? 'Residents' : $category->name;
        $data = array(
          'page_class' => 'wrapper wrapper--index',
          'page_title' => $category_name.' | The Word Radio',
          'page_category' => $category_name,
          'page_description' => 'The Word Radio residents index',
          'page_name' => 'residents.index',
        );
        return view('templates/index-residents', compact('residents', 'data'));
      else:
        abort(404);
      endif;
    }



    /**
     * Show the article
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($slug){
      $resident = Cache::remember('resident'.' '.$slug, $this->minutes, function() use($slug){
        return $resident = Resident::where('slug', $slug)->first();
      });
      if($resident):
        $data = array(
          'page_class' => 'resident'.' '.$resident->slug,
          'page_title' => $resident->title . ' | The Word Radio',
          'page_description' => str_limit($resident->text, 300),
          'page_name' => 'resident.show',
        );
        return view('templates/resident', compact('resident', 'data'));
      else:
        abort(404);
      endif;
    }
}
