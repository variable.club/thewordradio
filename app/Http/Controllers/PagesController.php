<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Page;
use App\Setting;
use Illuminate\Support\Facades\Cache;

class PagesController extends FrontController{

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct(){
    $this->minutes = 48*60; // 48h
    parent::__construct();
  }
  /**
   * Show the article
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function show($slug = 0){
    $article = Cache::remember('page'.' '.$slug, $this->minutes, function() use($slug){
      return $event = page::where('slug', $slug)->first();
    });

    $data = array(
      'page_class' => 'wrapper wrapper--detail'.' '.$article->slug,
      'page_title' => $article->title . ' | The Word Radio',
      'page_description' => str_limit($article->text, 300),
      'page_name' => 'pages.show',
    );
    return view('templates/page', compact('article', 'data'));

    // $articles = Page::where('parent_id', 0)
    //               ->where('published', 1)
    //               ->orderBy('order', 'asc')
    //               ->get();
    // if($slug === 0){
    //   $article = $articles->first();
    // }else{
    //   $article = Page::whereTranslation('slug', $slug)->first();
    // }
    // $data = array(
    //   'page_class' => 'article'.' '.$article->slug,
    //   'page_title' => $article->title,
    //   'page_description' => str_limit($article->text, 300),
    //   'page_name' => 'pages.show',
    // );
    // return view('templates/page', compact('article', 'articles', 'data'));
  }


  /**
   * Show the about page
   *
   * @return \Illuminate\Http\Response
   */

  public function about(){
    $article = Page::find(1);
    $data = array(
      'page_class' => 'wrapper wrapper--detail'.' '.$article->slug,
      'page_title' => $article->title . ' | The Word Radio',
      'page_description' => str_limit($article->text, 300),
      'page_name' => 'pages.show',
    );
    return view('templates/page', compact('article', 'data'));
  }

}
