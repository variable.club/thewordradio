<?php

namespace App\Http\Controllers;
use App\Article;
use App\Show;
use App\Resident;
use App\Event;
use App\Video;
use App\Page;
use DB;
use Illuminate\Http\Request;
use Spatie\Searchable\Search;
use Validator;
use Parsedown;

class SearchController extends FrontController
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct(){
    $this->minutes = 48*60; // 48h
    parent::__construct();
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
   public function search(Request $request){
     $keywords = strip_tags($request->input('q'));
     $data = array(
       'page_class' => 'wrapper wrapper--results',
       'page_title' => 'Search | The Word Radio',
       'page_description' => '',
       'page_name' => 'search',
     );

     $residents = null;
     $shows = null;
     $tracklists = null;
     $events = null;
     $videos = null;
     $articles = null;
     $pages = null;

     if(!empty($keywords)):

       $validator = Validator::make($request->all(), [
          'q' => 'required|max:150',
        ]);

        if($validator->fails()) {
          $keywords = 'Invalid search';
        }else{
          $keywords = $request->input('q');
        }

       // Query Residents
       $residents = Resident::where(function($query) use ($keywords){
           $query->where('title', 'like', '%' . $keywords . '%')
                 ->orWhere('text', 'like', '%' . $keywords . '%');
         })
         ->where('published', 1)
         ->orderBy('title')
         ->take(50)
         ->get();

       // Query Shows
       $shows = Show::where(function($query) use ($keywords){
           $query->where('title', 'like', '%' . $keywords . '%')
                 ->orWhere('text', 'like', '%' . $keywords . '%');
         })
         ->where('published', 1)
         ->orderBy('created_at', 'desc')
         ->take(30)
         ->get();

       // Query tracklist
       $tracklists = Show::where('tracklist', 'like', '%' . $keywords . '%')
         ->where('published', 1)
         ->orderBy('created_at', 'desc')
         ->take(30)
         ->get();
       // Parse trakclist to get one row only
       if(!empty($tracklists)):

         // Parse markdown text
         $tracklists->map(function ($item, $key) use($keywords) {
           $Parsedown = new Parsedown();
           $tracklist = $Parsedown->text($item->tracklist);
           $tracklist_array = !empty($tracklist) ? ul_to_array($tracklist) : '';
           $item['track'] = !empty($tracklist_array) ? li_search($tracklist_array, $keywords) : '';
         });
       endif;

       // Query Events
       $events = DB::table('events as a')
        ->leftJoin('events as b', 'b.parent_id', '=', 'a.id')
        ->where(function($query) use ($keywords){
            $query->where('a.title','like','%'.$keywords.'%')
                    ->orWhere('a.text','like','%'.$keywords.'%')
                    ->orWhere('b.title','like','%'.$keywords.'%')
                    ->orWhere('b.text','like','%'.$keywords.'%')
                    ->orWhere('b.place','like','%'.$keywords.'%');
          })
        ->where('a.published', 1)
        ->where('b.published', 1)
        ->select('a.*')
        ->where('a.parent_id', 0)
        ->distinct()
        ->get();
        // Remove descendant events
        $events = $events->where('parent_id', 0);

        // Query Article
        $articles = Article::where(function($query) use ($keywords){
           $query->where('title', 'like', '%' . $keywords . '%')
               ->orWhere('intro', 'like', '%' . $keywords . '%')
               ->orWhere('text', 'like', '%' . $keywords . '%');
        })
        ->where('published', 1)
        ->orderBy('created_at', 'desc')
        ->take(50)
        ->get();

        // Query Videos
        $videos = Video::where(function($query) use ($keywords){
           $query->where('title', 'like', '%' . $keywords . '%')
               ->orWhere('text', 'like', '%' . $keywords . '%');
        })
        ->where('published', 1)
        ->orderBy('created_at', 'desc')
        ->take(50)
        ->get();

        // Query pages
        $pages = Page::where(function($query) use ($keywords){
           $query->where('title', 'like', '%' . $keywords . '%')
               ->orWhere('text', 'like', '%' . $keywords . '%');
        })
        ->where('published', 1)
        ->where('id', [1,2])
        ->get();
       endif;
     return view('templates/search', compact('residents', 'shows', 'tracklists', 'events', 'articles', 'videos', 'data', 'keywords', 'pages'));
   }

   /**
    * Collect
    *
    * @return collection
    */

     public function mergeCollect($collection1, $collection2){
       // Merge collections - don't use merge without toBase() since if there is an artilce and an event with the same id, one will overwrite the other
       $merged_collection = $collection1->toBase()->merge($collection2)->sortBy('created_at');
       return $merged_collection;
     }
}
