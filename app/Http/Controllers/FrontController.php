<?php

namespace App\Http\Controllers;
use App\Setting;
use App\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

class FrontController extends Controller
{
  public function __construct(){
    $this->minutes = 43200; // 30 days
    // Analytics
    $google_analytics = Cache::remember('google-analytics', $this->minutes, function(){
      return $google_analytics = Setting::find(2);
    });
    $google_analytics_content = (!empty($google_analytics->content)) ? $google_analytics->content : '' ;
    View::share('google_analytics', $google_analytics_content);
    // Banner
    $banners = Cache::remember('banners', $this->minutes, function(){
      $today = Carbon::today();
      return $banners = Banner::where('published', 1)
                              ->whereDate('start_date','<=', $today)
                              ->whereDate('end_date','>=', $today)
                              ->get();
    });
    View::share('banners', $banners);
  }
}
