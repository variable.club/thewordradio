<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class bannersController extends FrontController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
      $this->minutes = 48*60; // 48h
      parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    /**
     * Show the article
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($slug){
      $article = Cache::remember('banner'.' '.$slug, $this->minutes, function() use($slug){
        return $article = banner::where('slug', $slug)->first();
      });
      if($article):
        $data = array(
          'page_class' => 'banner'.' '.$article->slug,
          'page_title' => $article->title,
          'page_description' => str_limit($article->text, 300),
          'page_name' => 'banner.show',
        );
        return view('templates/banner', compact('article', 'data'));
      else:
        abort(404);
      endif;
    }
}
