<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Show;
use App\Taxonomy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class showsController extends FrontController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
      $this->minutes = 48*60; // 48h
      parent::__construct();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
      $page = request()->has('page') ? request()->get('page') : 1;
      $shows = Cache::remember('shows-index'.'-'.$page, $this->minutes, function(){
        return $shows = Show::where('published', 1)
                              ->orderBy('start_date', 'desc')
                              ->paginate(48);
        });
      $data = array(
        'page_class' => 'wrapper wrapper--index',
        'page_title' => 'Shows | The Word Radio',
        'page_category' => '',
        'page_description' => 'The Word Radio shows index',
        'page_name' => 'shows.index',
      );
      $paginate = true;
      return view('templates/index-shows', compact('shows', 'data', 'paginate'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexByTag($category){

      $category = Taxonomy::where('slug', $category)->first();
      if(!empty($category)):
        $shows = Cache::remember('shows-index'.'-'.$category->slug, $this->minutes, function() use($category){
          return $shows = $category->showsByTaxonomy;
          });
        $category_name = ($category->slug == 'the-word') ? '' : ucfirst($category->name);
        $data = array(
          'page_class' => 'wrapper wrapper--index',
          'page_title' => $category_name.  ' Shows | The Word Radio',
          'page_category' => $category_name,
          'page_description' => 'The Word '.$category_name.' Radio shows index',
          'page_name' => 'shows.index',
        );
        return view('templates/index-shows', compact('shows', 'data'));
      else:
        abort(404);
      endif;
    }


    /**
    * Show the article
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    public function show($resident_slug, $slug){
    $show = Cache::remember('show'.' '.$slug, $this->minutes, function() use($slug){
      return $show = show::where('slug', $slug)->first();
    });
    if($show):
      $data = array(
        'page_class' => 'show'.' '.$show->slug,
        'page_title' => $show->resident->title .' - '. $show->title.' | The Word Radio',
        'page_description' => str_limit($show->text, 300),
        'page_name' => 'shows.show',
      );
      return view('templates/show', compact('show', 'data'));
    else:
      abort(404);
    endif;
    }
}
