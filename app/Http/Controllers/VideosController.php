<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Setting;
use App\Video;
use App\Taxonomy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class videosController extends FrontController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
      $this->minutes = 48*60; // 48h
      parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index($category = 'in-studio', $parent_slug = '')
     {
       // dd($parent_slug);
       $category = Taxonomy::where('slug', $category)->first();
       if(!empty($category)):
         if(empty($parent_slug)):
           $videos = Cache::remember('articles-videos-'.$category->slug, $this->minutes, function() use($category){
             return $videos = $category->videosByTaxonomy;
           });
           $page_name = $category->name;
        else:
          $parent = Cache::remember('video'.' '.$parent_slug, $this->minutes, function() use($parent_slug){
            return $video = video::where('slug', $parent_slug)->first();
          });
          $videos = Cache::remember('articles-videos-'.$parent_slug, $this->minutes, function() use($parent){
            return $videos = $parent->children;
          });
          $page_name = $parent->title;
        endif;
         $data = array(
           'page_class' => 'wrapper wrapper--index',
           'page_title' => 'Videos ' . $page_name .' | The Word Radio',
           'page_category' => $page_name,
           'page_description' => 'The Word Radio videos index',
           'page_name' => 'videos.index',
         );
         return view('templates/index-videos', compact('videos', 'data', 'category'));
       else:
         abort(404);
       endif;
     }


    /**
     * Show the article
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($slug){
      $video = Cache::remember('video'.' '.$slug, $this->minutes, function() use($slug){
        return $video = video::where('slug', $slug)->first();
      });
      if($video):
        $data = array(
          'page_class' => 'video'.' '.$video->slug,
          'page_title' => $video->title.' | The Word Radio',
          'page_description' => str_limit($video->text, 300),
          'page_name' => 'video.show',
        );
        return view('templates/video', compact('video', 'data'));
      else:
        abort(404);
      endif;
    }
}
