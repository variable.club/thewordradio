<?php
namespace App\Http\Traits;
use App\Taxonomy;
use App\Show;


trait TaxonomyTrait {

    /**
    * Get all of the taxonomies for the post.
    */

    public function taxonomies(){
      return $this->morphToMany('App\Taxonomy', 'taxonomyable');
    }


    /**
    * Retourne une liste de catégories associées à l'article
    * Nécessaire pour le dropdown select
    *
    */

    // public function getTaxonomiesAttribute() {
    //   return $this->taxonomies->pluck('id')->all();
    // }



   /**
   * Get the category
   */

   public function residentCategory(){
     return $this->morphToMany('App\Taxonomy', 'taxonomyable')
                 ->where('parent_id', 679);
   }


   /**
    * Get the residents by taxonomy
   *
    */

   public function residentsByTaxonomy(){
     if($this->slug == 'specials'):
       $residents = $this->morphedByMany('App\Resident', 'taxonomyable')
                        ->where('published', 1)
                        ->orderBy('created_at', 'desc');
     else:
       $residents = $this->morphedByMany('App\Resident', 'taxonomyable')
                        ->where('published', 1)
                        ->orderBy('title');
     endif;
     return $residents;
   }



   /**
    * Get the shows by taxonomy
   *
    */

   public function showsByTaxonomy(){
     return $this->morphedByMany('App\Show', 'taxonomyable')
                      ->where('published', 1)
                      ->orderBy('start_date', 'desc')
                      ->limit(100);
   }


   /**
    * Get the vieos by taxonomy
   *
    */

   public function videosByTaxonomy(){
     return $this->morphedByMany('App\Video', 'taxonomyable')
                    ->orderBy('created_at', 'desc')
                    ->where('published', 1);
   }

   /**
   * Get the tags
   */

   public function tags(){
     return $this->morphToMany('App\Taxonomy', 'taxonomyable')
                 ->where('parent_id', 1);
   }

   /**
    * Returns the categories for a select
    *
    */

   public function taxonomiesDropdown($parent_id, $appendEmpty=0){
     if($appendEmpty){
       return Taxonomy::where('parent_id', $parent_id)->get()->pluck('name', 'id')->prepend('', '');
     }else{
       return Taxonomy::where('parent_id', $parent_id)->get()->pluck('name', 'id');
     }
   }
}
