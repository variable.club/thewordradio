<?php
namespace App\Http\Traits;
use App\Resident;
use App\Show;


trait ResidentTrait {

    /**
     * Get the Resident
     *
     */

    public function resident(){
      return $this->belongsTo('App\Resident');
    }


    /**
     * Get the shows
     *
     */

    public function shows(){
      return $this->hasMany('App\Show')->orderBy('start_date', 'desc');
    }

   /**
    * Returns the categories for a select
    *
    */

   public function residentsDropdown(){
     return Resident::orderBy('title')->get()->pluck('title', 'id')->prepend('', '');
   }
}
