<?php

namespace App\Console\Commands;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Str;

// use Illuminate\Console\Command;

class GenerateMigration extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'make:custom-migration';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a variable custom migration';


    /**
    * The type of class being generated.
    *
    * @var string
    */
    protected $type = 'Job';

    // Location of your custom stub
    protected function getStub()
    {
      $folder = (env('APP_MULTILINGUAL')) ? 'Multilingual' : 'Unilingual';
      return  app_path().'/Console/Commands/Stubs/'. $folder .'/CreateMigration.stub';
    }


    /**
    * Get the console command options.
    *
    * @return array
    */
    protected function getOptions()
    {
        return [
            ['table', null, InputOption::VALUE_OPTIONAL, 'The database name to use.'],
        ];
    }


    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getPath($name)
    {
        $name = Str::replaceFirst($this->rootNamespace(), '', $name);
        return base_path().'/database/migrations/'.date('Y_m_d').'_0000_'.str_replace('\\', '/', $name).'.php';
    }

    /**
    * Build the class with the given name.
    *
    * @param  string  $name
    * @return string
    */

    protected function buildClass($name)
    {
        $stub = $this->files->get($this->getStub());
        $class_name = ucfirst(Str::camel(trim($this->input->getArgument('name'))));
        // Replace the table name
        if(!empty($this->option('table'))):
            $stub = str_replace(
                ['DummyTable', 'DummyClassName'],
                [$this->option('table'), $class_name],
                $stub
            );
        endif;
        return $this->replaceNamespace($stub, $name)->replaceClass($stub, $name);
    }

}
