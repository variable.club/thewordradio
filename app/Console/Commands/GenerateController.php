<?php
namespace App\Console\Commands;
use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;


class GenerateController extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'make:custom-controller';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a variable custom controller';


    /**
    * The type of class being generated.
    *
    * @var string
    */
    protected $type = 'Job';

    // Location of your custom stub
    protected function getStub()
    {
      $folder = (env('APP_MULTILINGUAL')) ? 'Multilingual' : 'Unilingual';
      return  app_path().'/Console/Commands/Stubs/'. $folder .'/FrontController.stub';
    }


    // The root location the file should be written to
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace;
    }


    /**
     * Replace the namespace for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return $this
     */
    protected function replaceNamespace(&$stub, $name)
    {
        $class = str_replace($this->getNamespace($name).'\\', '', $name);
        $stub = str_replace(
            ['DummyNamespace', 'DummyRootNamespace', 'NamespacedDummyUserModel', 'DummySingular'],
            [$this->option('table'), $this->rootNamespace(), config('auth.providers.users.model'), Str::singular($this->option('table'))],
            $stub
        );
        return $this;
    }



        /**
        * Get the console command options.
        *
        * @return array
        */
        protected function getOptions()
        {
            return [
                ['table', null, InputOption::VALUE_OPTIONAL, 'The database name to use.'],
            ];
        }


    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getPath($name)
    {
        $name = ucfirst(str_replace_first($this->rootNamespace(), '', $name));
        return $this->laravel['path'].'/Http/Controllers/'.str_replace('\\', '/', $name).'.php';
    }

}
