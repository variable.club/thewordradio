<?php

namespace App\Http\Controllers\Admin;
use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DummyUpperSingular;
use App\Media;
use App\Taxonomy;
use DB;
use Carbon\Carbon;
use Lang;
use App\Http\Requests\Admin\ArticleRequest;
use DataTables;
use Illuminate\Support\Facades\Input;


class DummyClass extends AdminController
{

  public function __construct(){
    $this->table_type = 'DummyNamespace';
    Lang::setLocale(config('app.locale'));
    $this->middleware(['auth', 'permissions'])->except('index');
    // Define an array of all related taxonomie's parents
    $this->taxonomies = [];
    parent::__construct();
  }

  /**
   * List all articles
   *
   * @param  string  $parent_id
   * @return \Illuminate\Http\Response
   */

  public function index(){
    $articles = DummyUpperSingular::orderBy('order', 'asc')
                  ->orderBy('created_at', 'desc')
                  ->get();
    $data = array(
      'page_class' => 'index',
      'page_title' => 'DummyNamespace',
      'page_id'    => 'index-DummyNamespace',
      'table_type' => $this->table_type,
    );
    return view('admin/templates/DummyNamespace-index', compact('articles', 'data'));
  }


  /**
   * Get articles for datatables (ajax)
   *
   * @return \Illuminate\Http\Response
   */

  public function getDataTable(){
    return \DataTables::of(DummyUpperSingular::withTranslation()
                        ->get())
                        ->addColumn('title_link', function ($article) {
                          return '<div class="text-content"><a href="' . route('admin.DummyNamespace.edit', $article->id) . '" class="text-content">' . $article->title . '</a></div>';
                        })
                        ->addColumn('action', function ($article) {
                          return '<a href="' . route('admin.DummyNamespace.edit', $article->id) . '" class="link">' . __('admin.edit') . '</a>';
                        })
                        ->rawColumns(['title_link', 'action'])
                        ->make(true);
  }


  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function edit($id){
    $article = DummyUpperSingular::findOrFail($id);
    $data = array(
      'page_class' => 'DummyNamespace',
      'page_title' => 'DummyNamespace edit',
      'page_id'    => 'edit-DummySingular',
      'table_type' => $this->table_type,
    );
    // dd($article->categories);
  	return view('admin/templates/DummySingular-edit',  compact('article', 'data'));
  }


  /**
   * Show the form for creating a new resource.
   *
   * @param  string  $parent_slug
   * @return \Illuminate\Http\Response
   */

  public function create(){
    $data = array(
      'page_class' => 'DummyUpperSingular create',
      'page_title' => 'DummySingular create',
      'page_id'    => 'create-DummySingular',
      'table_type' => $this->table_type,
    );
    $article = new DummyUpperSingular;
    return view('admin.templates.DummySingular-edit', compact('article', 'data'));
  }


  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   */

  public function update(DummyUpperSingular $DummySingular, ArticleRequest $request){
    // Save article
    return $this->saveObject($DummySingular, $request, $this->taxonomies);
  }


  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */

  public function store(ArticleRequest $request){
    // Create article
    return $this->createObject(DummyUpperSingular::class, $request, 'redirect', $this->taxonomies);
  }


  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function destroy(DummyUpperSingular $DummySingular){
    return $this->destroyObject($DummySingular);
  }


  /**
   * Reorder the articles relative to parent
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return Json response
   */

   public function reorder(Request $request){
     return $this->orderObject(DummyUpperSingular::class, $request);
   }
}
