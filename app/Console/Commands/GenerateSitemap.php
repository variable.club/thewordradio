<?php

namespace App\Console\Commands;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;

class GenerateSitemap extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'sitemap:GenerateSitemap';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'GenerateSitemap description';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
      parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
      $this->info('sitemap generate started..');
      // Sitemap::create()
      //   ->add(Url::create('/')
      //       ->setLastModificationDate(Carbon::yesterday())
      //       ->setChangeFrequency(Url::CHANGE_FREQUENCY_YEARLY)
      //       ->setPriority(0.1))
      //
      //   ->writeToFile(public_path('sitemap_test.xml');
      Sitemap::create()
      // home
      ->add(Url::create('http://theword.radio/')
          ->setLastModificationDate(Carbon::yesterday())
          ->setChangeFrequency(Url::CHANGE_FREQUENCY_DAILY)
          ->setPriority(0.8))
      // schedule
      ->add(Url::create('http://theword.radio/schedule')
          ->setLastModificationDate(Carbon::yesterday())
          ->setChangeFrequency(Url::CHANGE_FREQUENCY_DAILY)
          ->setPriority(0.8))
      // residents
      ->add(Url::create('http://theword.radio/residents')
          ->setLastModificationDate(Carbon::yesterday())
          ->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)
          ->setPriority(0.8))
      // shows
      ->add(Url::create('http://theword.radio/shows')
          ->setLastModificationDate(Carbon::yesterday())
          ->setChangeFrequency(Url::CHANGE_FREQUENCY_DAILY)
          ->setPriority(0.8))
      // agenda
      ->add(Url::create('http://theword.radio/agenda')
          ->setLastModificationDate(Carbon::yesterday())
          ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
          ->setPriority(0.8))
      // interviews
      ->add(Url::create('http://theword.radio/interviews')
          ->setLastModificationDate(Carbon::yesterday())
          ->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)
          ->setPriority(0.8))
      // guests
      ->add(Url::create('http://theword.radio/residents/category/guests')
          ->setLastModificationDate(Carbon::yesterday())
          ->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)
          ->setPriority(0.8))
      // specials
      ->add(Url::create('http://theword.radio/residents/category/specials')
          ->setLastModificationDate(Carbon::yesterday())
          ->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)
          ->setPriority(0.5))
      // Video in-studio
      ->add(Url::create('http://theword.radio/videos/category/in-studio')
          ->setLastModificationDate(Carbon::yesterday())
          ->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)
          ->setPriority(0.5))
      // Video in Situ
      ->add(Url::create('http://theword.radio/videos/category/in-situ')
          ->setLastModificationDate(Carbon::yesterday())
          ->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)
          ->setPriority(0.5))
      // about
      ->add(Url::create('http://theword.radio/about')
          ->setLastModificationDate(Carbon::yesterday())
          ->setChangeFrequency(Url::CHANGE_FREQUENCY_YEARLY)
          ->setPriority(0.5))
      ->writeToFile(public_path('sitemap.xml'));
  }
}
