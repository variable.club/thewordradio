<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Http\Traits\MediaTrait;
use Cviebrock\EloquentSluggable\Sluggable;

class Page extends Model{
  use MediaTrait;
  use Sluggable;
  protected $table = 'pages';
  protected $fillable = ['title', 'intro', 'text', 'slug', 'created_at', 'order', 'published', 'parent_id'];


  /**
   * Construct : default Locale
   *
   */

  public function __construct(array $attributes = []){
    parent::__construct($attributes);
    $this->addMediaCollection('une', __('admin.featured_image'))->singleFile();
  }


  /**
   * Return the sluggable configuration array for this model.
   *
   * @return array
   */

  public function sluggable(){
    return [
      'slug' => [
        'source' => 'title',
        'onUpdate' => true
      ]
    ];
  }


  /**
   * Add a title if not set
   * @param string  $order
  *
   */

   public function setTitleAttribute($value){
     if(!isset($value) or $value == ''):
       $this->attributes['title'] = null;
     else:
       $this->attributes['title'] = $value;
     endif;
   }

  /**
   * Get children articles
   *
   */

  public function children(){
    return $this->hasMany('App\Page', 'parent_id')
                ->orderBy('order', 'asc');
  }


  /**
   * Get children articles
   *
   */

  public function parent(){
    return $this->belongsTo('App\Page', 'parent_id');
  }

  /**
   * Réécrit la date d'update
   * @param string  $value (date)
	 *
   */
  public function getUpdatedAtAttribute($value){
    if(!empty($value)){
      Carbon::setLocale(config('app.locale'));
      $date = Carbon::parse($value)->diffForHumans();
    }else{
      $date = "";
    }
    return $date;
  }



  /**
   * Set default parent_id
   *
   */

  public function setParentIdAttribute($parent_id){
    if (empty($parent_id)){
      $this->attributes['parent_id'] = 0;
    }else{
      $this->attributes['parent_id'] = $parent_id;
    }
  }


  /**
   * Concact mdoel + title for related dropdown
   * @param date  $date
   *
   */

   public function getModelTitleAttribute(){
     $data = get_class().', '.$this->id;
     return $data;
   }
}
