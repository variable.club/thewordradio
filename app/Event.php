<?php
// Variable
namespace App;
use App\Http\Traits\MediaTrait;
use App\Http\Traits\TaxonomyTrait;
use App\MediaCollection;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use MediaTrait;
    use TaxonomyTrait;
    use Sluggable;
    protected $table = 'events';
    protected $fillable = ['title', 'start_date', 'end_date', 'intro', 'place', 'text', 'slug', 'created_at', 'order', 'published', 'featured', 'parent_id'];

    /**
    * Construct
    *
    */

    public function __construct(array $attributes = []){
      parent::__construct($attributes);
      $this->addMediaCollection('une', __('admin.featured_image'))->singleFile();
    }


    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */

    public function sluggable(){
      return [
        'slug' => [
          'source' => 'title',
          'onUpdate' => true
        ]
      ];
    }


    /**
     * Get children articles
     *
     */

    public function children(){
      return $this->hasMany('App\Event', 'parent_id')
                  ->orderBy('order', 'asc');
    }


    /**
     * Get children articles
     *
     */

    public function publishedChildren(){
      return $this->hasMany('App\Event', 'parent_id')
                  ->where('published', 1)
                  ->orderBy('order', 'asc');
    }

    /**
     * Get children articles
     *
     */

    public function parent(){
      return $this->belongsTo('App\Event', 'parent_id');
    }


    /**
    * Get the ID based on the Slug
    * @param string  $title
    *
    */

    public static function getSlugFromId($id){
      $article = Event::find($id);
      if($article){
        return $article->slug;
      }
    }


    /**
    * GET: Format 'updated_at'
     * @param string  $value (date)
     *
     */
    public function getUpdatedAtAttribute($value){
      if(!empty($value)){
        Carbon::setLocale(config('app.locale'));
        $date = Carbon::parse($value)->diffForHumans();
      }else{
        $date = "";
      }
      return $date;
    }


    /**
    * Create formated date-time
    */

    public function dateTime($date){
      if(!empty($date)){
        return Carbon::createFromFormat('d.m.Y', $date)->format('Y-m-d');
      }else{
        return null;
      }
    }


    /**
    * Concact model + title for related dropdown
    *
    */

    public function getModelTitleAttribute(){
      $data = get_class().', '.$this->id;
      return $data;
    }


}
