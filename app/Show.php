<?php
// Variable
namespace App;
use App\Http\Traits\MediaTrait;
use App\Http\Traits\TaxonomyTrait;
use App\Http\Traits\ResidentTrait;
use App\MediaCollection;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Show extends Model
{
    use MediaTrait;
    use TaxonomyTrait;
    use ResidentTrait;
    use Sluggable;
    protected $table = 'shows';
    protected $fillable = ['title', 'intro', 'text', 'tracklist', 'slug', 'created_at', 'start_date', 'order', 'published', 'artist_id', 'featured', 'sound_url'];

    /**
    * Construct
    *
    */

    public function __construct(array $attributes = []){
      parent::__construct($attributes);
      $this->addMediaCollection('une', __('admin.featured_image'))->singleFile();
    }


    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */

    public function sluggable(){
      return [
        'slug' => [
          'source' => 'title',
          'onUpdate' => true
        ]
      ];
    }


    /**
    * Get the ID based on the Slug
    * @param string  $title
    *
    */

    public static function getSlugFromId($id){
      $article = Show::find($id);
      if($article){
        return $article->slug;
      }
    }


    /**
    * GET: Format 'updated_at'
     * @param string  $value (date)
     *
     */
    public function getUpdatedAtAttribute($value){
      if(!empty($value)){
        Carbon::setLocale(config('app.locale'));
        $date = Carbon::parse($value)->diffForHumans();
      }else{
        $date = "";
      }
      return $date;
    }




    /**
    * Create formated date-time
    */

    public function dateTime($date){
      if(!empty($date)){
        return Carbon::createFromFormat('d.m.Y', $date)->format('Y-m-d');
      }else{
        return null;
      }
    }


    /**
    * Concact model + title for related dropdown
    *
    */

    public function getModelTitleAttribute(){
      $data = get_class().', '.$this->id;
      return $data;
    }


}
