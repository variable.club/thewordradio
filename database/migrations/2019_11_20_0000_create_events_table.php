<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
          $table->increments('id');
          $table->timestamps();
          $table->integer('order')->default(0);
          $table->string('title');
          $table->integer('parent_id')->nullable()->default(0);
          $table->string('slug');
          $table->text('intro')->nullable();
          $table->text('text')->nullable();
          $table->unique(['slug']);
          $table->date('start_date')->nullable();
          $table->date('end_date')->nullable();
          $table->string('place')->nullable();
          $table->boolean('published')->default(false);
          $table->boolean('featured')->default(false);
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
