<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shows', function (Blueprint $table) {
          $table->increments('id');
          $table->timestamps();
          $table->integer('order')->default(0);
          $table->string('title');
          $table->string('slug');
          $table->string('sound_url')->nullable();
          $table->text('intro')->nullable();
          $table->text('text')->nullable();
          $table->text('tracklist')->nullable();
          $table->unique(['slug']);
          $table->boolean('published')->default(false);
          $table->boolean('featured')->default(false);
          $table->date('start_date')->nullable();
          $table->integer('resident_id')->unsigned()->nullable();
          $table->foreign('resident_id')->references('id')->on('residents');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shows');
    }
}
