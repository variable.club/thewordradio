<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
          $table->increments('id');
          $table->timestamps();
          $table->integer('order')->default(0);
          $table->string('title');
          $table->string('url')->nullable();
          $table->string('slug');
          $table->text('intro')->nullable();
          $table->text('text')->nullable();
          $table->unique(['slug']);
          $table->boolean('published')->default(false);
          $table->boolean('featured')->default(false);
          $table->date('start_date')->nullable();
          $table->date('end_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
