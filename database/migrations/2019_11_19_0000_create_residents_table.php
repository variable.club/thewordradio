<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('residents', function (Blueprint $table) {
          $table->increments('id');
          $table->timestamps();
          $table->integer('order')->default(0);
          $table->string('title');
          $table->string('slug');
          $table->text('intro')->nullable();
          $table->text('text')->nullable();
          $table->unique(['slug']);
          $table->boolean('published')->default(false);
          $table->boolean('featured')->default(false);
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('residents');
    }
}
