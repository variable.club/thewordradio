<?php

use Illuminate\Database\Seeder;
use App\Article;

class Articles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


      // Add 1 article
      $faker = Faker\Factory::create();
      Article::create([
        'title' => $faker->unique()->word,
        'text' => $faker->text($maxNbChars = 800),
        'parent_id' => 0,
        'published' => 1,
        'order' => 0,
      ]);

    }
}
