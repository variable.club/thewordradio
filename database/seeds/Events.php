<?php

use Illuminate\Database\Seeder;
use App\Event;

class Events extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        Event::create([
          'title' => 'An event group',
          'published' => 1,
          'parent_id' => 0,
          'start_date' => new DateTime,
          'end_date' => new DateTime,

        ]);

        Event::create([
          'title' => $faker->unique()->word,
          'text' => $faker->text($maxNbChars = 800),
          'start_date' => new DateTime,
          'published' => 1,
          'parent_id' => 1,
        ]);

        Event::create([
          'title' => $faker->unique()->word,
          'text' => $faker->text($maxNbChars = 800),
          'start_date' => new DateTime,
          'published' => 1,
          'parent_id' => 1,
        ]);

    }
}
