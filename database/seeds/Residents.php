<?php

use Illuminate\Database\Seeder;
use App\Resident;

class Residents extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


      // Add 1 article
      $faker = Faker\Factory::create();
      Resident::create([
        'title' => $faker->unique()->word,
        'text' => $faker->text($maxNbChars = 800),
        'published' => 1,
        'order' => 0,
      ]);

      Resident::create([
        'title' => $faker->unique()->word,
        'text' => $faker->text($maxNbChars = 800),
        'published' => 1,
        'order' => 0,
      ]);
    }
}
