<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $this->call(Articles::class);
      $this->call(UsersTableSeeder::class);
      $this->call(Taxonomies::class);
      $this->call(Pages::class);
      $this->call(SettingsTableSeeder::class);
      $this->call(Residents::class);
      $this->call(Shows::class);
      $this->call(Videos::class);
      $this->call(Events::class);
    }
}
