<?php

use Illuminate\Database\Seeder;
use App\Taxonomy;

class Taxonomies extends Seeder
{

      /**
       * Run the database seeds.
       *
       * @return void
       */
      public function run()
      {
        Taxonomy::create([
          'id' => 679,
          'name' => 'Residents',
          'parent_id' => 0,
          'order' => 0,
        ]);

        Taxonomy::create([
          'id' => 680,
          'name' => 'Events',
          'parent_id' => 0,
          'order' => 0,
        ]);

        Taxonomy::create([
          'id' => 681,
          'name' => 'Videos',
          'parent_id' => 0,
          'order' => 0,
        ]);

        Taxonomy::create([
          'name' => 'The Word',
          'parent_id' => 679,
          'order' => 0,
        ]);

        Taxonomy::create([
          'name' => 'Guests',
          'parent_id' => 679,
          'order' => 1,
        ]);

        Taxonomy::create([
          'name' => 'Specials',
          'parent_id' => 679,
          'order' => 2,
        ]);

        Taxonomy::create([
          'name' => 'Music',
          'parent_id' => 680,
          'order' => 0,
        ]);

        Taxonomy::create([
          'name' => 'Art',
          'parent_id' => 680,
          'order' => 1,
        ]);

        Taxonomy::create([
          'name' => 'In studio',
          'parent_id' => 681,
          'order' => 1,
        ]);

        Taxonomy::create([
          'name' => 'In situ',
          'parent_id' => 681,
          'order' => 1,
        ]);
      }

}
