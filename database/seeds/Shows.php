<?php

use Illuminate\Database\Seeder;
use App\Show;

class Shows extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


      // Add 1 article
      $faker = Faker\Factory::create();
      Show::create([
        'title' => $faker->unique()->word,
        'text' => $faker->text($maxNbChars = 800),
        'published' => 1,
        'resident_id' => 1,
        'order' => 0,
        'start_date' => new DateTime,
      ]);

      Show::create([
        'title' => $faker->unique()->word,
        'text' => $faker->text($maxNbChars = 800),
        'published' => 1,
        'resident_id' => 2,
        'order' => 0,
        'start_date' => new DateTime,
      ]);
    }
}
