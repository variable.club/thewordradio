<?php

use Illuminate\Database\Seeder;
use App\Video;

class Videos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        Video::create([
          'title' => 'A group of video',
          'published' => 1,
          'parent_id' => 0,
        ]);

        Video::create([
          'title' => $faker->unique()->word,
          'url' => 'https://www.youtube.com/watch?v=ET1Lkupg7h8',
          'published' => 1,
          'parent_id' => 0,
        ]);

        Video::create([
          'title' => $faker->unique()->word,
          'url' => 'https://www.youtube.com/watch?v=D26PxPRRrUA',
          'published' => 1,
          'parent_id' => 1,
        ]);

    }
}
