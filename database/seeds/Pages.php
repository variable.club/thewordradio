<?php

use Illuminate\Database\Seeder;
use App\Page;

class Pages extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Page::create([
          'title' => 'About',
          'published' => 1,
          'parent_id' => 0,
        ]);


    }
}
