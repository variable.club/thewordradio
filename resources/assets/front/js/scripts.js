/*------------------------------------*\
  #SCRIPTS
\*------------------------------------*/

var $ = require("jquery");

$( document ).ready(function() {
  // modules
  require('./modules/_lazysizes.js');
  require('./modules/_isTouched.js');
  require('./modules/_ajax.js');
  require('./modules/_player.js');
  require('./modules/_menu.js');
  require('./modules/_liveInfos.js');
  require('./modules/_schedule.js');
  require('./modules/_randomGrid.js');
  require('./modules/_galleryScroll.js');
  require('./modules/_masonry.js');
  require('./modules/_bannerSwitch.js');
  require('./modules/_links.js');
  require('./modules/_embeds.js');
});
