/*------------------------------------*\
  masonry
\*------------------------------------*/

var Masonry = require('masonry-layout');
var imagesLoaded = require('imagesloaded');

function masonryGrid() {
  var grid = document.querySelector('.index--agenda .index__content');
  if (grid) {
    var msnry = new Masonry(grid, {
      itemSelector: '.block',
      percentPosition: true,
      transitionDuration: 0
    });
    // relaunch after all images are loaded
    imagesLoaded( grid ).on( 'progress', function() {
      msnry.layout();
    });
    document.addEventListener('lazyloaded', function(e){
      msnry.layout();
    });
  }
}

// launch it
masonryGrid();
// relaunch it on every page load
document.addEventListener("pjax:success", masonryGrid);