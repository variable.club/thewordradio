/*------------------------------------*\
  ajax
\*------------------------------------*/

var Pjax = require("pjax");

var pjax = new Pjax({
  elements: "a[href], form[action]",
  cacheBust: false,
  selectors: [
    "title",
    "meta",
    ".banner",
    "#main",
    ".header .nav"
  ],
})
