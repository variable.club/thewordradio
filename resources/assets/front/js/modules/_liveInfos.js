/*------------------------------------*\
  Live infos
\*------------------------------------*/
var $ = require("jquery");
var moment = require("moment");

$.ajax({
  url: "https://thewordradio.airtime.pro/api/live-info-v2",
  dataType: 'json',
  success: function(results){
    // Current Show
    var currentStarts = moment(results.shows.current.starts, "YYYY-MM-DD HH:mm:ss").format('HH:mm');
    var currentEnds = moment(results.shows.current.ends, "YYYY-MM-DD HH:mm:ss").format('HH:mm');
    var currentName = results.shows.current.name;
    var fullCurrentName = currentName + ' (' + currentStarts + ' - ' + currentEnds + ')';
    $('.live__current .live__text').append(fullCurrentName);
    // Next show
    var nextStarts = moment(results.shows.next[0].starts, "YYYY-MM-DD HH:mm:ss").format('HH:mm');
    var nextEnds = moment(results.shows.next[0].ends, "YYYY-MM-DD HH:mm:ss").format('HH:mm');
    var nextName = results.shows.next[0].name;
    var fullNextName = nextName + ' (' + nextStarts + ' - ' + nextEnds + ')';
    $('.live__next .live__text').append(fullNextName);
  }
});
