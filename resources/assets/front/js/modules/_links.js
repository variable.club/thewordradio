/*------------------------------------*\
 Links
\*------------------------------------*/

var $ = require("jquery");

function extLinks() {
$(".body a").click(function(e) {
  e.preventDefault();
  window.open(this.href);
});
}

// launch it
extLinks();
// relaunch it on every page load
document.addEventListener("pjax:success", extLinks);
