/*------------------------------------*\
  banner switch
\*------------------------------------*/
var $ = require("jquery");
var bannerImg = $('.banner__img'), src;

function bannerSwitch() {
  var windowWidth = window.innerWidth;
  if (windowWidth >= 768) {
    bannerImg.each(function () {
      src = bannerImg.attr('data-desktop');
      bannerImg.attr("src", src);
    });
  } else {
    bannerImg.each(function () {
      src = bannerImg.attr('data-mobile');
      bannerImg.attr("src", src);
    });
  }
}

// launch it
bannerSwitch();
// relaunch it on resize
window.onresize = function () { bannerSwitch(); };
// relaunch it on every page load
document.addEventListener("pjax:success", bannerSwitch);
