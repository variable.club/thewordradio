/*------------------------------------*\
  Schedule
\*------------------------------------*/
var $ = require("jquery");
var moment = require("moment");

function schedule() {
  if ($('.schedule').length) {
    $.ajax({
      url: "https://thewordradio.airtime.pro/api/week-info?timezone=Europe/Brussels",
      dataType: 'json',
      success: function(results){
        $.each(results , function(index, val) {
          var dayShows = val;
          var dayDate = moment(val[0].start_timestamp, "YYYY-MM-DD HH:mm:ss").format('dddd DD.MM');
          var s = '<article class="schedule__day"><h2 class="schedule__title">'+ dayDate +'</h2><ul class="schedule__list">';
          $.each(dayShows , function(index, val) {
            var showName = val.name;
            var showStarts = moment(val.start_timestamp, "YYYY-MM-DD HH:mm:ss").format('HH:mm');
            var showEnds = moment(val.end_timestamp, "YYYY-MM-DD HH:mm:ss").format('HH:mm');
            s += '<li class="schedule__item"><time class="schedule__time">'+ showStarts +' - '+ showEnds +'</time><span class="schedule__show">'+ showName +'</span></li>';
          });
          s += '</ul></article>';
          $('.schedule').append(s);
        });
      }
    })
  }
}

// launch it
schedule();
// relaunch it on every page load
document.addEventListener("pjax:success", schedule);
