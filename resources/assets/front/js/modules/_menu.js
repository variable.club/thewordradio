/*------------------------------------*\
  menu
\*------------------------------------*/

var $ = require("jquery");

function menuInteraction() {
  // var
  var $header = $('.header--primary');
  var $menuItem = $('.nav__trigger');
  var $subList = $('.nav__list--sub');
  // item click
  $menuItem.click(function(e) {
    var $currentNav = $(this).parents('.nav__item').find($subList);
    if ($currentNav.is('.is-active')) {
      $subList.removeClass('is-active');
    } else {
      $subList.removeClass('is-active');
      $currentNav.addClass('is-active');
    }
  });
  // elsewhere click
  $(document).mouseup(function(e) {
    if (!$header.is(e.target) && $header.has(e.target).length === 0) {
      $subList.removeClass('is-active');
    }
  });
}
// launch it
menuInteraction();
// relaunch it on every page load
document.addEventListener("pjax:success", menuInteraction);
