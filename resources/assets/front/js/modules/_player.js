/*------------------------------------*\
  player
\*------------------------------------*/

var $ = require("jquery");
var $liveTrigger = $('.live__trigger');

// live
$liveTrigger.click(function(e) {
  if ($liveTrigger.is('.is-active')) {
    livePause();
  } else {
    playerStop();
    $('#live')[0].play();
    $liveTrigger.addClass('is-active');
  }
});
function livePause() {
  $('#live')[0].pause();
  $liveTrigger.removeClass('is-active');
}
function playerStop() {
  $('#player').removeClass('is-active');
  $('#player iframe').attr('src', '');
}

// player
function playerTrigger() {
  $('.player-trigger').click(function(e) {
    livePause();
    var mixcloudId = this.getAttribute( "data-show" );
    var mixcloudUrl = 'https://www.mixcloud.com/widget/iframe/?hide_cover=1&autoplay=1&mini=1&light=1&hide_artwork=0&feed=/' + mixcloudId;
    $('#player iframe').attr('src', mixcloudUrl);
    $('#player').addClass('is-active');
    e.preventDefault();
  });
  $('.player__close').click(function(e) {
    playerStop();
    e.preventDefault();
  });
}
// launch it
playerTrigger();
// relaunch it on every page load
document.addEventListener("pjax:success", playerTrigger);
