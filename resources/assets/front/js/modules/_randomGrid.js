/*------------------------------------*\
  Random grid
\*------------------------------------*/

var $ = require("jquery");
var displace = require('displacejs');

function randomGrid() {
  var gridSelector = $('#randomGrid');
  if (gridSelector.length) {
    gridSelector.find('.block').each(function() {
      var block = $(this);
      randomPosition(block);
      draggable(block);
    });
  }
}

function randomPosition(block) {
  var containerH = $('#randomGrid').height(),
      containerW = $('#randomGrid').width(),
      containerPaddingT = $('.index--mixed .index__inner').css("padding-top").replace("px", ""),
      blockW     = block.width(),
      blockH     = block.height(),
      maxW       = containerW - blockW,
      maxH       = containerH - blockH,
      blockLeft  = Math.floor( Math.random() * Math.floor(maxW) ),
      blockTop   = Math.floor( Math.random() * Math.floor(maxH) );
  if (blockH > containerH) {
    $('.index--mixed .index__inner').css('height', blockH + containerPaddingT * 2 + 2);
  }
  if (blockTop < 0) {blockTop = 0;}
  block.css({
    'position' : 'absolute',
    'left'     : blockLeft,
    'top'      : blockTop
  });
}

var indexHighest = 1;
function draggable(block) {
  var block = block[0];
  var options = {
    constrain: true,
    handle: block.querySelector('.block__media'),
    relativeTo: document.querySelector('#randomGrid'),
    onMouseDown: active,
    onTouchStart: active,
    onMouseUp: inactive,
    onTouchStop: inactive
  };
  function active(el) {
    $(el).addClass('is-moving');
    $(el).css('z-index' , indexHighest);
  }
  function inactive(el){
  	$(el).removeClass('is-moving');
    indexHighest++;
  }
  displace(block, options);
}

// launch it
randomGrid();
// relaunch it on every page load
document.addEventListener("pjax:success", randomGrid);
