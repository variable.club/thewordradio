/*------------------------------------*\
  Gallery Scroll
\*------------------------------------*/

var $ = require("jquery");

function galleryScroll() {
  var gallery = $('.gallery--scroll');
  if (gallery.length) {
    var windowHeight = $(window).height();
    var textHeight = $('.gallery-scroll-text').outerHeight();
    if (textHeight/1.1 > windowHeight) {
      var galleryItems = gallery.find('.gallery__item');
      var itemCount = galleryItems.length;
      var pageHeight = $('.detail__section:first-child').outerHeight();
      var animationFrame = Math.floor((pageHeight - windowHeight) / itemCount);
      $( window ).scroll(function() {
        var windowScrollCount = Math.round($(this).scrollTop());
        for ( var i = 0; i < itemCount; i++) {
          var itemFrame = animationFrame * i;
          var item = galleryItems[i];
          if (windowScrollCount > itemFrame) {
            $(item).addClass('is-active');
          } else {
            $(item).removeClass('is-active');
          }
        }
      });
    } else {
      gallery.addClass('is-classic');
    }
  }
}

// launch it
galleryScroll();
// relaunch it on every page load
document.addEventListener("pjax:success", galleryScroll);
