<article class="block block--connect">
  <div class="block__inner">
    <section class="block__text">
      <h2 class="block__title">Subscribe to our newsletter</h2>
      <form id="newsletter-form" action="https://variable-club.createsend.com/t/r/s/dtzduk/" method="post" class="form form--newsletter">
        <div class="input-group">
          <label for="newsletter">Subscribe to our newsletter</label>
          <input title="Email address" id="email" name="cm-dtzduk-dtzduk" type="email" value="" placeholder="Email address" required autocorrect="off" autocapitalize="off">
          <button class="button"><div class="button__inner">Subscribe</div></button>
        </div>
      </form>
      <h2 class="block__title">Follow us</h2>
      <section class="socials">
        <ul class="socials__list">
          <li class="socials__item"><a href="https://www.facebook.com/TheWordMagazine/" target="_blank" class="socials__link icon icon--facebook">Facebook</a></li>
          <li class="socials__item"><a href="https://www.instagram.com/thewordradio/" target="_blank" class="socials__link icon icon--instagram">Instagram</a></li>
          <li class="socials__item"><a href="https://www.mixcloud.com/TheWordMagazine/" target="_blank" class="socials__link icon icon--mixcloud">Mixcloud</a></li>
          {{-- <li class="socials__item"><a href="#" target="_blank" class="socials__link icon icon--twitter">Twitter</a></li> --}}
        </ul>
      </section>
      <h2 class="block__title">Stay hydrated</h2>
    </section>
  </div>
</article>
