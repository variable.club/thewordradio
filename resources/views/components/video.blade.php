<div class="embed-wrapper">
  <div class='embed-container'>
    <iframe src='{{ \App\Helpers\VideoHelper::get_url_embed($url) }}' frameborder='0' allowfullscreen allow="autoplay; encrypted-media"></iframe>
  </div>
</div>
