{{-- @if (str_contains($media->mime_type, "image/gif") or str_contains($media->mime_type, "image/png"))
<img src="{{ url('/imagecache/original') }}/{{ $media->file_name }}" alt="{{ $media->alt }}" class="image-lazy lazyload" />
@else --}}
<img
  data-sizes="auto"
  src="{{ url('/imagecache/small') }}/{{ $src }}"
  data-src="{{ url('/imagecache/medium') }}/{{ $src }}"
  data-srcset="{{ url('/imagecache/small') }}/{{ $src }} 800w,
  {{ url('/imagecache/medium') }}/{{ $src }} 1200w,
  {{ url('/imagecache/large') }}/{{ $src }} 1600w"
  alt="{{ $alt }}"
  class="image-lazy lazyload" />
{{-- @endif --}}
