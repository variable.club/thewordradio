@if(!empty($show) && !empty($show->resident))
<article class="block block--show">
  <div class="block__inner">
    <section class="block__media">
      <a href="{{ route('shows.show', [$show->resident->slug, $show->slug]) }}" class="block__link">
      @if(count($show->getMedias('une')))
        @foreach($show->getMedias('une') as $media)
        @include('components.image', ['src' => $media->file_name, 'alt' => $media->alt])
        @endforeach
      @elseif(count($show->resident->getMedias('une')))
        @foreach($show->resident->getMedias('une') as $media)
        @include('components.image', ['src' => $media->file_name, 'alt' => $media->alt])
        @endforeach
      @endif
      </a>
      @if($show->sound_url)
      <div class="player-trigger" data-show="{{ buildMixcloudUrl($show->sound_url) }}"></div>
      @endif
    </section>
    <section class="block__text">
      <a href="{{ route('shows.show', [$show->resident->slug, $show->slug]) }}" class="block__link">
        <h2 class="block__title">{{ $show->title }}</h2>
      </a>
      <time class="block__time">{{ formatedDate($show->start_date, 0) }}</time>
      @if(count($show->tags))
      <div class="tag">
        <ul class="tag__list">
          @foreach($show->tags as $tag)
          <li class="tag__item"><a href="{{ route('shows.indexbytags', [$tag->slug]) }}" class="tag__link">{{ $tag->name }}</a></li>
          @endforeach
        </ul>
      </div>
      @endif
    </section>
  </div>
</article>
@endif
