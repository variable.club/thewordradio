@if($links_array)
<section class="socials">
  <ul class="socials__list">
  @foreach($links_array as $link)
  @if(strpos($link, "http://") !== false or strpos($link, "https://") !== false)
    @switch($link)
      @case(strpos($link, 'facebook') !== false)
        @php $name  = 'Facebook'  @endphp
        @php $class = 'facebook'  @endphp
        @break
      @case(strpos($link, 'instagram') !== false)
        @php $name  = 'Instagram'  @endphp
        @php $class = 'instagram'  @endphp
        @break
      @case(strpos($link, 'twitter') !== false)
        @php $name  = 'Twitter'  @endphp
        @php $class = 'twitter'  @endphp
        @break
      @case(strpos($link, 'mixcloud') !== false)
        @php $name  = 'Mixcloud'  @endphp
        @php $class = 'mixcloud'  @endphp
        @break
      @case(strpos($link, 'soundcloud') !== false)
        @php $name  = 'Soundcloud'  @endphp
        @php $class = 'soundcloud'  @endphp
        @break
      @case(strpos($link, 'bandcamp') !== false)
        @php $name  = 'Bandcamp'  @endphp
        @php $class = 'bandcamp'  @endphp
        @break
      @default
      @php $name= 'Website'  @endphp
      @php $class = 'url'  @endphp
    @endswitch
    <li class="socials__item"><a href="{{ $link }}" class="socials__link icon icon--{{ $class }}" target="_blank">{{ $name }}</a></li>
  @endif
  @endforeach
  </ul>
</section>
@endif
