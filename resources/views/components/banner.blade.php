@if(count($banners))
  @php $banner = $banners->random() @endphp
  @if(count($banner->getMedias('desktop')))
  <section class="banner">
    <div class="banner__inner">
      <div class="banner__media">
        <a href="{{ $banner->url }}" class="banner__link" target="_blank">
          <img class="banner__img"
          src="{{ url('/imagecache/original') }}/{{ $banner->getMedias('desktop')->first()->file_name }}"
          data-desktop="{{ url('/imagecache/original') }}/{{ $banner->getMedias('desktop')->first()->file_name }}"
          @if(count($banner->getMedias('mobile')))
            data-mobile="{{ url('/imagecache/original') }}/{{ $banner->getMedias('mobile')->first()->file_name }}"
          @endif
          alt="{{ $banner->title }}" />
        </a>
      </div>
    </div>
  </section>
  @endif
@endif
