@if(count($resident->getMedias('une')))
<article class="block block--resident">
  <div class="block__inner">
    <section class="block__media">
      <a href="{{ route('residents.show', [$resident->slug]) }}" class="block__link">
      @foreach($resident->getMedias('une') as $media)
        @include('components.image', ['src' => $media->file_name, 'alt' => $media->alt])
      @endforeach
      </a>
    </section>
    <section class="block__text">
      <a href="{{ route('residents.show', [$resident->slug]) }}" class="block__link">
        <h2 class="block__title">{{ $resident->title }}</h2>
      </a>
    </section>
  </div>
</article>
@endif
