@include('components.banner')
<header class="header header--primary" id="header">
  <div class="header__inner">
    <div class="header__topbar">
      <div class="logo">
        <a href="{{ route('homepage') }}" class="logo__link">
          <img src="/assets/front/images/theword.svg" alt="The Word Radio">
        </a>
      </div>
      <div class="live">
        <div class="live__trigger"></div>
        <div class="live__info">
          <div class="info__inner">
            <h3 class="live__current"><span class="live__label">Now</span><span class="live__text"></span></h3>
            <h3 class="live__next"><span class="live__label">Next</span><span class="live__text"></span></h3>
          </div>
        </div>
        <audio class="live__audio" id="live" src="https://thewordradio.out.airtime.pro/thewordradio_a"></audio>
      </div>
      <a href="{{ route('search') }}" class="search-link"></a>
    </div>
    <nav class="nav">
      <ul class="nav__list">
        <li class="nav__item nav__item--listen">
          <div class="nav__trigger">Listen</div>
          <ul class="nav__list nav__list--sub">
            <li class="nav__item"><a href="{{ route('residents.index') }}" class="nav__link">Residents</a></li>
            <li class="nav__item"><a href="{{ route('shows.index') }}" class="nav__link">Shows</a></li>
            <li class="nav__item"><a href="{{ route('residents.indexbycategory', ['guests']) }}" class="nav__link">Guests</a></li>
            <li class="nav__item"><a href="{{ route('residents.indexbycategory', ['specials']) }}" class="nav__link">Specials</a></li>
            <li class="nav__item"><a href="{{ route('schedule') }}" class="nav__link">Schedule</a></li>
          </ul>
        </li>
        <li class="nav__item nav__item--read">
          <div class="nav__trigger">Read</div>
          <ul class="nav__list nav__list--sub">
            <li class="nav__item"><a href="{{ route('events.index') }}" class="nav__link">Agenda</a></li>
            <li class="nav__item"><a href="{{ route('articles.index') }}" class="nav__link">Interviews</a></li>
          </ul>
        </li>
        <li class="nav__item nav__item--watch">
          <div class="nav__trigger">Watch</div>
          <ul class="nav__list nav__list--sub">
            <li class="nav__item"><a href="{{ route('videos.index', ['in-studio']) }}" class="nav__link">In studio</a></li>
            <li class="nav__item"><a href="{{ route('videos.index', ['in-situ']) }}" class="nav__link">In situ</a></li>
          </ul>
        </li>        <li class="nav__item nav__item--connect">
          <div class="nav__trigger">Connect</div>
          <ul class="nav__list nav__list--sub">
            <li class="nav__item"><a href="{{ route('pages.show', ['contact']) }}" class="nav__link">Contact</a></li>
            {{-- <li class="nav__item"><a href="" class="nav__link">Subscribe</a></li> --}}
            <li class="nav__item"><a href="{{ route('pages.about') }}" class="nav__link">About</a></li>
          </ul>
        </li>
      </ul>
    </nav>
  </div>
</header>
