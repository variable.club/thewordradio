<div class="tag">
  <ul class="tag__list">
    @foreach($tags as $tag)
      <li class="tag__item"><a href="{{ route('shows.indexbytags', [$tag->slug]) }}" class="tag__link">{{ $tag->name }}</a></li>
    @endforeach
  </ul>
</div>
