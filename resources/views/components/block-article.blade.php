@if(count($article->getMedias('une')))
<article class="block block--interview">
  <div class="block__inner">
    <section class="block__media">
      <a href="{{ route('articles.show', [$article->slug]) }}" class="block__link">
      @foreach($article->getMedias('une') as $media)
        @include('components.image', ['src' => $media->file_name, 'alt' => $media->alt])
      @endforeach
      </a>
    </section>
    <section class="block__text">
      <a href="{{ route('articles.show', [$article->slug]) }}" class="block__link">
        <time class="block__time">{{ formatedDate($article->created_at) }}</time>
        <h2 class="block__title">{{ $article->title }}</h2>
      </a>
    </section>
  </div>
</article>
@endif
