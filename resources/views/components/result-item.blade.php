<li class="result__item">
  <section class="result__text">
    <a href="{{ $link }}" class="result__link">
      <h2 class="result__title">{{ $title }}</h2>
    </a>
    <time class="result__time">{{ $date }}</time>
    {{-- {{> components-tag }} --}}
  </section>
</li>
