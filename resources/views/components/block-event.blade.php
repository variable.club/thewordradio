<article class="block block--event">
  <div class="block__inner">
    <section class="block__media">
        @if(count($event_child->getMedias('une'))) @foreach($event_child->getMedias('une') as $media)
          @include('components.image', ['src' => $media->file_name, 'alt' => $media->alt])
        @endforeach @endif
      </section>
    <section class="block__text">
      @if(count($event_child->taxonomies))
      <div class="block__category">{{ $event_child->taxonomies->first()->name }}</div>
      @endif
      <h2 class="block__title">{{ $event_child->title }}</h2>
      <p class="block__location">{{ $event_child->place }}</p>
      <time class="block__time">
        {{ formatedDate($event_child->start_date, 0)}}
        @if($event_child->end_date)
        → {{ formatedDate($event_child->end_date, 0) }}
        @endif
      </time>
      <div class="block__excerpt body">
        @markdown($event_child->text)
      </div>
    </section>
  </div>
</article>
