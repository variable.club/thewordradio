@if($video->children->count())
  @php $link = route('videos.index', [$category->slug, $video->slug]) @endphp
@else
  @php $link = route('videos.show', [$video->slug]) @endphp
@endif
@if(count($video->getMedias('une')))
<article class="block block--video">
  <div class="block__inner">
    <section class="block__media">
      <a href="{{ $link }}" class="block__link">
        @foreach($video->getMedias('une') as $media)
          @include('components.image', ['src' => $media->file_name, 'alt' => $media->alt])
        @endforeach
        @if(empty($video->children->count()))
        <div class="video-icon"></div>
        @endif
      </a>
    </section>
    <section class="block__text">
      <a href="{{ $link }}" class="block__link">
        <h2 class="block__title">{{ $video->title }}</h2>
      </a>
      <time class="block__time">{{ formatedDate($video->created_at) }}</time>
      @if(count($video->tags))
        @include('components.tags', ['tags' => $video->tags])
      @endif
    </section>
  </div>
</article>
@endif
