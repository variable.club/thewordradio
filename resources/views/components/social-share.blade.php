<a
  href="https://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}/&t={{ $title }}"
  onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
  target="_blank">
  ↗ Share on Facebook
</a>
<a
  href="https://twitter.com/share?url={{ url()->current() }}&text={{ $title }}"
  onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
  target="_blank">
  ↗ Share on Twitter
</a>
