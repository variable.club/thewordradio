<li class="result__item">
  <section class="result__text">
    <a href="{{ $link }}" class="result__link">
      <h3 class="track__title">{{ $track }} <span>↓</span></h3>
      <h2 class="result__title">{{ $title }}</h2>
    </a>
    <time class="result__time">{{ $date }}</time>
  </section>
</li>
