@if($event->children->count())
  @php $link = route('events.show', [$event->slug]) @endphp
@else
  @php $link = route('events.single.show', [$event->slug]) @endphp
@endif
@if(count($event->getMedias('une')))
<article class="block block--agenda">
  <div class="block__inner">
    <section class="block__media">
      <a href="{{ $link }}" class="block__link">
      @foreach($event->getMedias('une') as $media)
        @include('components.image', ['src' => $media->file_name, 'alt' => $media->alt])
      @endforeach
      </a>
    </section>
    <section class="block__text">
      <a href="{{ $link }}" class="block__link">
        <h2 class="block__title">{{ $event->title }}</h2>
      </a>
      <time class="block__time">
        {{ formatedDate($event->start_date, 0)}}
        @if($event->end_date)
        → {{ formatedDate($event->end_date, 0) }}
        @endif
      </time>
    </section>
  </div>
</article>
@endif
