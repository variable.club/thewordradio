@if (!empty($paginate) && $paginator->hasPages())
  <nav aria-label="pagination" class="pagination">
    <ul class="pagination__list">
      {{-- Previous Page Link --}}
      @if (!$paginator->onFirstPage())
        <li><a href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="Previous" class="pagination__link">Previous</a></li>
      @endif

      @if($paginator->currentPage() > 3)
        <li class="hidden-small"><a href="{{ $paginator->url(1) }}" class="pagination__link">1</a></li>
      @endif
      @if($paginator->currentPage() > 4)
        <li class="hidden-small"><span>...</span></li>
      @endif
      @foreach(range(1, $paginator->lastPage()) as $i)
        @if($i >= $paginator->currentPage() - 2 && $i <= $paginator->currentPage() + 2)
          @if ($i == $paginator->currentPage())
            <li class="active"><span>{{ $i }}</span></li>
          @else
            <li class="hidden-small"><a href="{{ $paginator->url($i) }}"  class="pagination__link">{{ $i }}</a></li>
          @endif
        @endif
      @endforeach
      @if($paginator->currentPage() < $paginator->lastPage() - 3)
        <li class="hidden-small"><span>...</span></li>
      @endif
      @if($paginator->currentPage() < $paginator->lastPage() - 2)
          <li class="hidden-small"><a href="{{ $paginator->url($paginator->lastPage()) }}" class="pagination__link">{{ $paginator->lastPage() }}</a></li>
      @endif

      {{-- Next Page Link --}}
      @if ($paginator->hasMorePages())
        <li><a href="{{ $paginator->nextPageUrl() }}" rel="next" class="pagination__link">Next</a></li>
      @endif
    </ul>
  </nav>
@endif
