@extends('admin.app')

@section('content')
  @if ($errors->has('name'))
    <div class="help-block">
      <span>{{ $errors->first('name') }}</span>
    </div>
  @endif
  @if ($errors->has('email'))
    <div class="help-block">
      <span>{{ $errors->first('email') }}</span>
    </div>
  @endif
  @if ($errors->has('password'))
    <div class="help-block">
      <span>{{ $errors->first('password') }}</span>
    </div>
  @endif

  <div class="wrapper wrapper--small panel panel-default">
      <div class="panel-heading">Register</div>
      <div class="panel-body">
          <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
              {{ csrf_field() }}
              <div class="form__content">
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">Name</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">Password</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" required>

                    </div>
                </div>

                <div class="form-group">
                    <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                </div>
              </div>
              <button type="submit" class="btn btn-primary">
                  Register
              </button>
          </form>
      </div>
</div>
@endsection
