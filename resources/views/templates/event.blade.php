@extends('app')

@section('page_title', $data['page_title'])
@section('page_class', $data['page_class'])
@section('page_description', strip_tags($data['page_description']))
  @if($event->getMedias('une'))
  @foreach($event->getMedias('une') as $media)
    @section('page_image_url',  url('/imagecache/small').'/'.$media->file_name)
  @endforeach
@endif

@section('content')
  <main class="main" id="main">
    <section class="index index--agenda">
      <div class="index__inner">
        <h2 class="index__title">
          {{ $event->title }}
          {{-- @if($event->start_date && $event->end_date)
            From
            {{ \Carbon\Carbon::createFromFormat('Y-m-d', $event->start_date)->toFormattedDateString() }}
            to
            {{ \Carbon\Carbon::createFromFormat('Y-m-d', $event->end_date)->toFormattedDateString() }}
          @elseif($event->start_date)
            From {{ \Carbon\Carbon::createFromFormat('Y-m-d', $event->start_date)->toFormattedDateString() }}
          @endif --}}
        </h2>
        <div class="index__content">
          @if($event->publishedChildren) @foreach($event->publishedChildren as $event_child)
          @include('components.block-event')
          @endforeach @endif
        </div>
      </div>
    </section>
  </main>
@endsection
