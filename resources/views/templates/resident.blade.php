@extends('app')

@section('page_title', $data['page_title'])
@section('page_class', $data['page_class'])
@section('page_description', strip_tags($data['page_description']))
@if($resident->getMedias('une'))
  @foreach($resident->getMedias('une') as $media)
    @section('page_image_url',  url('/imagecache/small').'/'.$media->file_name)
  @endforeach
@endif

@section('content')
  <main class="main" id="main">
    <article class="detail detail--resident">
      <section class="detail__section">
        <header class="header">
          <h1 class="detail__title">{{ $resident->title }}</h1>
        </header>
        <div class="detail__intro body">
          @markdown($resident->text)
        </div>
        @if(count($resident->shows))
        <section class="index index--shows">
          <div class="index__inner">
            <h2 class="index__title">Shows</h2>
            <div class="index__content">
              @foreach($resident->shows as $show)
              @include('components.block-show')
              @endforeach
            </div>
          </div>
        </section>
        @endif
      </section>
      <section class="detail__section">
        {{-- Picture --}}
        @if(count($resident->getMedias('une'))) @foreach($resident->getMedias('une') as $media)
        <section class="gallery gallery--basic">
          <figure class="gallery__item">
            @include('components.image', ['src' => $media->file_name, 'alt' => $media->alt])
          </figure>
        </section>
        @endforeach @endif
        {{-- Links --}}
        @if($resident->intro)
        @php $links_array = explode("\n", $resident->intro); @endphp
        @include('components.block-links')
        @endif
      </section>
    </article>
  </main>
@endsection
