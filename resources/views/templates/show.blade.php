@extends('app')

@section('page_title', $data['page_title'])
@section('page_class', $data['page_class'])
@section('page_description', strip_tags($data['page_description']))
@if($show->getMedias('une'))
  @foreach($show->getMedias('une') as $media)
    @section('page_image_url',  url('/imagecache/small').'/'.$media->file_name)
  @endforeach
@elseif(count($show->resident->getMedias('une')))
  @foreach($show->resident->getMedias('une') as $media)
    @section('page_image_url',  url('/imagecache/small').'/'.$media->file_name)
  @endforeach
@endif

@section('content')
  <main class="main" id="main">
    <article class="detail detail--show">
      <section class="detail__section">
        <header class="header">
          @if($show->sound_url)
          <div class="player-trigger" data-show="{{ buildMixcloudUrl($show->sound_url) }}"></div>
          @endif
          <div class="header__text">
            <h1 class="detail__title">{{ $show->title }}</h1>
            <time class="detail__time">{{ formatedDate($show->start_date, 0) }}</time>
            @if(count($show->tags))
            <div class="tag">
              <ul class="tag__list">
                @foreach($show->tags as $tag)
                <li class="tag__item"><a href="{{ route('tags', [$tag->slug]) }}" class="tag__link">{{ $tag->name }}</a></li>
                @endforeach
              </ul>
            </div>
            @endif
          </div>
        </header>
        @if($show->text)
        <div class="detail__intro body">
          @markdown($show->text)
        </div>
        @endif
        <div class="detail__tools">
          @if(!empty($show->resident))
          <a href="{{ route('residents.show', [$show->resident->slug]) }}">↙ Resident's page</a>
          @endif
          @include('components.social-share', ['title' => $show->title])
        </div>
        @if($show->tracklist)
        <div class="detail__content body">
          @markdown($show->tracklist)
        </div>
        @endif
      </section>
      <section class="detail__section">
        @if(count($show->getMedias('une')))
          @foreach($show->getMedias('une') as $media)
          <section class="gallery gallery--basic">
            <figure class="gallery__item">
              @include('components.image', ['src' => $media->file_name, 'alt' => $media->alt])
            </figure>
          </section>
        @endforeach
        @elseif(count($show->resident->getMedias('une')))
          @foreach($show->resident->getMedias('une') as $media)
          <section class="gallery gallery--basic">
            <figure class="gallery__item">
              @include('components.image', ['src' => $media->file_name, 'alt' => $media->alt])
            </figure>
          </section>
          @endforeach
        @endif
      </section>
    </article>
  </main>
@endsection
