@extends('app')

@section('page_title', $data['page_title'])
@section('page_class', $data['page_class'])
@section('page_description', strip_tags($data['page_description']))

@section('content')
  <main class="main" id="main">
    <section class="index index--shows">
      <div class="index__inner">
        <h2 class="index__title">{{ $data['page_category'] ?? '' }} Shows</h2>
        <div class="index__content">
          @if($shows) @foreach($shows as $show)
          @include('components.block-show')
          @endforeach @endif
        </div>
        @include('components.pagination', ['paginator' => $shows])
      </div>
    </section>
  </main>
@endsection
