@extends('app')

@section('page_title', $data['page_title'])
@section('page_class', $data['page_class'])
@section('page_description', strip_tags($data['page_description']))
@if($video->getMedias('une'))
  @foreach($video->getMedias('une') as $media)
    @section('page_image_url',  url('/imagecache/small').'/'.$media->file_name)
  @endforeach
@endif

@section('content')
  @if($video)
  <main class="main" id="main">
    <article class="detail detail--video">
      <section class="detail__section">
        <header class="header">
          <div class="header__text">
            <h1 class="detail__title">{{ $video->title }}</h1>
            <time class="detail__time">{{ formatedDate($video->created_at) }}</time>
            @if(count($video->tags))
              @include('components.tags', ['tags' => $video->tags])
            @endif
          </div>
        </header>
        @if($video->intro)
        <div class="detail__intro body">
          @markdown($video->intro)
        </div>
        @endif
        <div class="detail__tools">
          @include('components.social-share', ['title' => $video->title])
        </div>
        @if($video->text)
        <div class="detail__content body">
          @markdown($video->text)
        </div>
        @endif
      </section>
      @if($video->url)
      <section class="detail__section">
        @include('components.video', ['url' => $video->url])
      </section>
      @endif
    </article>
  </main>
  @endif
@endsection
