@extends('app')

@section('page_title', $data['page_title'])
@section('page_class', $data['page_class'])
@section('page_description', strip_tags($data['page_description']))

@section('content')
  <main class="main" id="main">
    <section class="index index--interviews">
      <div class="index__inner">
        <h2 class="index__title">Interviews</h2>
        <div class="index__content">
          @if($articles) @foreach($articles as $article)
          @include('components.block-article')
          @endforeach @endif
        </div>
      </div>
    </section>
  </main>
@endsection
