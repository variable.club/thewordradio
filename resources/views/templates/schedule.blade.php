@extends('app')

@section('page_title', 'Schedule | The Word Radio'),
@section('page_class', '')
@section('page_description', 'The Word Radio timetable')

@section('content')
  <main class="main" id="main">
    <article class="page page--schedule">
      <h2 class="page__title">Schedule</h2>
      <section class="schedule">
      </section>
    </article>
  </main>
@endsection
