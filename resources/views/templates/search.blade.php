@extends('app')

@section('page_title', $data['page_title'])
@section('page_class', $data['page_class'])
@section('page_description', strip_tags($data['page_description']))

@section('content')
  <main class="main" id="main">
    <article class="page page--search">
      <form class="form form--search" action="{{ route('search') }}">
        <div class="input-group">
          <label for="search">Search</label>
          <input type="text" name="q" placeholder="Search" value="{{ $keywords }}" autofocus onfocus="var temp_value=this.value; this.value=''; this.value=temp_value">
          <button class="button"><div class="button__inner">Search</div></button>
        </div>
      </form>
      @if($keywords)
      <section class="result">
        <section class="result__section listen">
          <h3 class="results__title">Residents</h3>
          <ul class="result__list">
            @if($residents->count())
              @foreach ($residents as $resident)
              @include('components.result-item-simple', ['title' => $resident->title, 'link' => route('residents.show', [$resident->slug])])
              @endforeach
            @else
              <li class="result__item">No result</li>
            @endif
          </ul>
          <h3 class="results__title">Shows</h3>
          <ul class="result__list">
            @if($shows->count())
              @foreach ($shows as $show)
              @include('components.result-item', ['title' => $show->title, 'link' => route('shows.show', [$show->resident->slug, $show->slug]), 'date' => formatedDate($show->start_date, 0)])
              @endforeach
            @else
              <li class="result__item">No result</li>
            @endif
          </ul>
          <h3 class="results__title">Tracks</h3>
          <ul class="result__list">
            @if($tracklists)
              @foreach ($tracklists as $tracklist)
              @include('components.result-item-track', ['title' => $tracklist->title, 'track' => $tracklist->track, 'link' => route('shows.show', [$tracklist->resident->slug, $tracklist->slug]), 'date' => formatedDate($tracklist->start_date, 0)])
              @endforeach
            @else
              <li class="result__item">No result</li>
            @endif
          </ul>
        </section>
        <section class="result__section read">
          <h3 class="results__title">Agenda</h3>
          <ul class="result__list">
            @if($events->count())
              @foreach ($events as $event)
              @include('components.result-item', ['title' => $event->title, 'link' => route('events.show', [$event->slug]), 'date' => formatedDate($event->start_date, 0)])
              @endforeach
            @else
              <li class="result__item">No result</li>
            @endif
          </ul>
          <h3 class="results__title">Interviews</h3>
          <ul class="result__list">
          @if($articles->count())
            @foreach ($articles as $article)
              @include('components.result-item', ['title' => $article->title, 'link' => route('articles.show', [$article->slug]), 'date' => formatedDate($article->created_at)])
            @endforeach
          @else
            <li class="result__item">No result</li>
          @endif
          </ul>
        </section>
        <section class="result__section watch">
          <h3 class="results__title">Watch</h3>
          <ul class="result__list">
            @if($videos->count())
              @foreach ($videos as $video)
                @if($video->children->count())
                  @php $link = route('videos.index', [$video->taxonomies->first()->slug, $video->slug]) @endphp
                @else
                  @php $link = route('videos.show', [$video->slug]) @endphp
                @endif
                @include('components.result-item', ['title' => $video->title, 'link' => $link, 'date' => formatedDate($video->created_at)])
              @endforeach
            @else
              <li class="result__item">No result</li>
            @endif
          </ul>
        </section>
        <section class="result__section connect">
          <h3 class="results__title">Watch</h3>
          <ul class="result__list">
          @if($pages->count())
            @foreach ($pages as $page)
            @include('components.result-item-simple', ['title' => $page->title, 'link' => route('pages.show', [$page->slug])])
            @endforeach
          @else
            <li class="result__item">No result</li>
          @endif
          </ul>
        </section>
      </section>
      @endif
    </article>
  </main>
@endsection
