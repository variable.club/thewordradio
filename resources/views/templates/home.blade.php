@extends('app')

@section('page_title', $data['page_title'])
@section('page_class', $data['page_class'])
@section('page_description', strip_tags($data['page_description']))

@section('content')
  <main class="main main--home" id="main">
    @if(count($featured_articles) or count($featured_shows) or count($featured_residents) or count($featured_events) or count($featured_videos))
    <section class="index index--mixed">
      <div class="index__inner">
        <div class="index__content" id="randomGrid">
          {{-- articles --}}
          @if(count($featured_articles)) @foreach($featured_articles as $article)
          @include('components.block-article')
          @endforeach @endif
          {{-- shows --}}
          @if(count($featured_shows)) @foreach($featured_shows as $show)
          @include('components.block-show')
          @endforeach @endif
          {{-- residents --}}
          @if(count($featured_residents)) @foreach($featured_residents as $resident)
          @include('components.block-resident')
          @endforeach @endif
          {{-- events --}}
          @if(count($featured_events)) @foreach($featured_events as $event)
          @include('components.block-event-parent')
          @endforeach @endif
          {{-- videos --}}
          @if(count($featured_videos)) @foreach($featured_videos as $video)
          @include('components.block-video')
          @endforeach @endif
          {{-- connect (newsletter) --}}
          @include('components.block-connect')
        </div>
      </div>
    </section>
    @endif
    @if(count($latest_shows))
    <section class="index index--shows">
      <div class="index__inner">
        <h2 class="index__title">Latest shows</h2>
        <div class="index__content">
        @foreach($latest_shows as $show)
        @include('components.block-show')
        @endforeach
        </div>
      </div>
    </section>
    @endif
  </main>
@endsection
