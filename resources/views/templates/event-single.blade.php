@extends('app')

@section('page_title', $data['page_title'])
@section('page_class', $data['page_class'])
@section('page_description', strip_tags($data['page_description']))
  @if($event->getMedias('une'))
  @foreach($event->getMedias('une') as $media)
    @section('page_image_url',  url('/imagecache/small').'/'.$media->file_name)
  @endforeach
@endif

@section('content')
  <main class="main" id="main">
    <section class="index index--agenda single">
      <div class="index__inner">
        <h2 class="index__title">
          {{ $event->title }}
        </h2>
        <div class="index__content">
          <article class="block block--event">
            <div class="block__inner">
              <section class="block__media">
                  @if(count($event->getMedias('une'))) @foreach($event->getMedias('une') as $media)
                    @include('components.image', ['src' => $media->file_name, 'alt' => $media->alt])
                  @endforeach @endif
                </section>
              <section class="block__text">
                @if(count($event->taxonomies))
                <div class="block__category">{{ $event->taxonomies->first()->name }}</div>
                @endif
                <div class="block__excerpt body">
                  @markdown($event->text)
                </div>
              </section>
            </div>
          </article>

        </div>
      </div>
    </section>
  </main>
@endsection
