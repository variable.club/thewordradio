@extends('app')

@section('page_title', $data['page_title'])
@section('page_class', $data['page_class'])
@section('page_description', strip_tags($data['page_description']))
@if($article->getMedias('une'))
  @foreach($article->getMedias('une') as $media)
    @section('page_image_url',  url('/imagecache/small').'/'.$media->file_name)
  @endforeach
@endif

@section('content')
  <main class="main" id="main">
    <article class="detail detail--page">
      <section class="detail__section">
        <header class="header">
          <h1 class="detail__title">{{ $article->title }}</h1>
        </header>
        <div class="detail__intro body">
          @markdown($article->intro)
        </div>
        <section class="socials">
          <ul class="socials__list">
            <li class="socials__item"><a href="https://www.facebook.com/TheWordMagazine/"target="_blank" target="_blank" class="socials__link icon icon--facebook">Facebook</a></li>
            <li class="socials__item"><a href="https://www.instagram.com/thewordradio/" target="_blank" target="_blank" class="socials__link icon icon--instagram">Instagram</a></li>
            <li class="socials__item"><a href="https://www.mixcloud.com/TheWordMagazine/" target="_blank" target="_blank" class="socials__link icon icon--mixcloud">Mixcloud</a></li>
          </ul>
        </section>
        <div class="detail__content body">
          @markdown($article->text)
        </div>
      </section>
      {{-- Picture --}}
      @if(count($article->getMedias('une'))) @foreach($article->getMedias('une') as $media)
      <section class="detail__section cover-wrapper">
      @include('components.image', ['src' => $media->file_name, 'alt' => $media->alt])
      </section>
      @endforeach @endif
    </article>
  </main>
@endsection
