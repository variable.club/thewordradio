@extends('app')

@section('page_url', Request::url())
@section('page_title', $data['page_title'])
@section('page_class', $data['page_class'])
@if($article->getMedias('une'))
  @foreach($article->getMedias('une') as $media)
    @section('page_image_url',  url('/imagecache/small').'/'.$media->file_name)
  @endforeach
@endif

@section('content')
  <main class="main" id="main">
    <article class="detail detail--interview">
      <section class="detail__section gallery-scroll-text">
        <header class="header">
          <h1 class="detail__title">{{ $article->title }}</h1>
        </header>
        @if($article->intro)
        <div class="detail__intro body">
          <p>{{ $article->intro }}</p>
        </div>
        @endif
        @if($article->author )
        <div class="detail__info">
          <div class="detail__author">{{ $article->author }} - {{ formatedDate($article->created_at) }}</div>
        </div>
        @endif
        <div class="detail__content body">
          @markdown($article->text)
        </div>
      </section>
      <section class="detail__section gallery-scroll-wrapper">
        @if(count($article->getMedias('gallery')))
          <section class="gallery gallery--scroll">
          @foreach($article->getMedias('gallery') as $media)
            <figure class="gallery__item">
              <img data-src="{{ url('/imagecache/medium') }}/{{ $media->file_name }}" class="image-lazy lazyload" alt="{{ $media->alt }}" />
            </figure>
        @endforeach
      </section>
      @endif
      </section>
    </article>
  </main>
@endsection
