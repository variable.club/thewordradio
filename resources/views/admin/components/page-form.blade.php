{!! Form::hidden('id', (!empty($article->id))? $article->id : '') !!}
{!! Form::hidden('parent_id', (!empty($article->parent) ? $article->parent->id : 0)) !!}
{!! Form::hidden('order', (!empty($article->order))? $article->order : 0) !!}


{{-- Validation errors --}}
@foreach ($errors->all() as $error)
    <span class="help-block">{{ $error }}</span>
@endforeach

  {{-- tabs --}}
  <div class="panel-heading">
    <div class="">

    </div>
    <div class="edit-details">
      @if(isset($article->id))
      <div class="created_at">
        {!! Form::text('created_at', $article->created_at->format('d.m.Y'), ['class' => 'form-control']) !!}
      </div>
      @endif
      {{-- Is published ? --}}
      <div class="is-published">
          <label>{!! Form::checkbox('published', 1, null) !!}<span>{{__('admin.published')}}</span></label>
      </div>
    </div>
  </div>
  <div class="panel-body">
    {{-- Title --}}
    <div class="form-group {!! $errors->has('title') ? 'has-error' : '' !!}">
      <label for="title">Title</label>
      {!! Form::text('title', (!empty($article->id) && !empty($article->title))? $article->title : old('title'), ['class' => 'form-control', 'placeholder' => 'Title']) !!}
      <span class="slug">
        @if(isset($article->slug))
        <i class="fa fa-link "></i>&nbsp;{{ (!empty($article->id) && !empty($article->slug)) ? $article->slug : '' }}
        @endif
      </span>
    </div>
    {{-- Intro --}}
    <div class="form-group {!! $errors->has('intro') ? 'has-error' : '' !!}">
      <label for="intro">Intro</label>
      {!! Form::textarea('intro', (!empty($article->id) && !empty($article->intro))? $article->intro : old('intro'), ['class' => 'form-control md-editor', 'placeholder' => 'Introduction', 'rows' => '5']) !!}
    </div>
    {{-- Text --}}
    <div class="form-group {!! $errors->has('text') ? 'has-error' : '' !!}">
      <label for="text">Text</label>
      {!! Form::textarea('text', (!empty($article->id) && !empty($article->text))? $article->text : old('text'), ['class' => 'form-control md-editor', 'id' => 'editor', 'placeholder' => 'Text']) !!}
    </div>
  </div>
@include('admin.components.form-submit')
