@foreach($article->mediaCollections as $collection)
  <?php $single_file = empty($collection->singleFile) ? false : $collection->singleFile; ?>
  @include('admin.components.media-panel', ['collection_name' => $collection->name, 'collection_name' => $collection->name, 'collection_title' => $collection->title, 'single_file' => $single_file])
@endforeach
{{-- !! Mandatory Modal --}}
@include('admin.components.media-edit-modal')

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {

    // ----- Medias panel display data----- //
    if( $('.media-panel').length ){
      $('.media-panel').each(function( index ) {
        var media_collection_name = $(this).attr('data-media-collection-name');
        // Build media list
        getMedias(media_collection_name);
      });
    }

    // ----- Medias panel add & upload ----- //
    // input file > open
    $(".media-add").click(function() {
      var input = $(this).prev();
      input.attr("type", "file");
      input.trigger('click');
      return false;
    });

    // Upload and store a media w/ Ajax
    var single_media_options = {
      success:  mediaResponse,
      error  :  mediaError,
      dataType: 'json'
    };

    $('body').delegate('.input-media-upload','change', function(){
      var panel = $(this).closest('.media-panel');
      var form = $(this).closest('.single-media-form');
      form.ajaxForm(single_media_options).submit();
      panel.addClass('loading');
    });

    function mediaResponse(response, statusText, xhr, $form){
      if(response.success){
        getMedias(response.collection_name);
      }
    }

    function mediaError(xhr, status, error){
      var err = JSON.parse(xhr.responseText);
      var errors = err.errors;
      for (var key in errors) {
        if (errors.hasOwnProperty(key)) {
          var msg = errors[key][0];
        }
      }
      $('<span class="help-block">' + msg + '</span>').appendTo('body').fadeOut(4000);

    }

    // ----- Media destroy ----- //
    var media_destroy_options = {
      success:       mediaDestroyResponse,
      dataType:      'json'
    };

    $( "body" ).on('click','.link--delete',function(e){
      e.preventDefault();
      $(this).closest('form').ajaxForm(media_destroy_options).submit();
    });

    function mediaDestroyResponse(response, statusText, xhr, $form){
      if(response.success){
        getMedias(response.collection_name);
      }
    }

    // ----- Media edit ----- //
    var media_edit_options = {
      success:       mediaEditResponse,
      dataType:      'json'
    };

    $(".media-edit-save").click(function(e) {
      e.preventDefault();
      $(this).closest('form').ajaxForm(media_edit_options).submit();
    });

    function mediaEditResponse(response, statusText, xhr, $form){
      if(response.success){
        getMedias(response.collection_name);
      }
    }


    // ----- Reorder medias ----- //
    if ( $('.panel.multiple').length ){
      // ----- Media gallery Sortable ----- //
      var el = $('.multiple .sortable');
      // var list = document.getElementById("sortable");
      $(el).each(function (i,e) {
        var sortable = Sortable.create(e, {
          onUpdate: function (evt) {
            var article_id = evt.item.getAttribute('data-article-id');
            var media_id   = evt.item.getAttribute('data-media-id');
            var collection_name = evt.item.getAttribute('data-media-collection-name');
            var new_order  = evt.newIndex;
            var model_name = '{{$data['table_type']}}';
            if (article_id && media_id) {
              jQuery.ajax({
                url: admin_url + '/medias/reorder/' + model_name + '/' +  article_id + '/' + collection_name,
                data: {
                  'mediaId' : media_id,
                  'newOrder': new_order,
                },
                type: 'POST',
                success: function(response){
                  if(response.status == 'success'){
                    getMedias(collection_name);
                    $('<span class="help-block">Updated</span>').appendTo('body').fadeOut(4000);
                  } else {
                  }
                }
              });
            }
          }
        });
      })
    }
  }) // End on document ready

  // ----- Mixins ----- //
  // ----- Manage data list ----- //
  function getMedias(media_collection_name) {
    // mediatable_type = typeof mediatable_type  === 'undefined' ? 'articles' : mediatable_type;
    // TODO: Afficher si aucun contenu
    var main_form_id = 'main-form';
    var article_id = $('#' + main_form_id + ' input[name=id]').val();
    var current_medias = $('#' + main_form_id + ' #' + media_collection_name).val();
    var panel = $("#panel-" + media_collection_name);
    var model_name = '{{$data['table_type']}}';
    // Get from DB
    if(article_id){
      $.ajax({
          dataType: 'JSON',
          url: admin_url+'/mediasArticle/' + model_name + '/' + article_id + '/' + media_collection_name,
      }).done(function(data){
        if(data.success == true){
          printList(data.medias, media_collection_name);
          if(data.medias.length == 0){
            panel.removeClass('hasmedia');
          }else{
            panel.addClass('hasmedia');
          }
          panel.removeClass('loading');
        }
      });
    }else{
      panel.removeClass('loading');
    }
  }


  // ----- Print the media ----- //
  function printList(medias, media_type) {
    var ul = $('#panel-' + media_type + ' .media-list');
    var	li = '';
    // Json Medias loop
    $.each( medias, function( key, value ) {
      if(medias && media_type){
        // Build <li>
        li = li + `
          <li
            class="list-group-item media-list__item"
            data-media-id="` + value.id + `"
            data-article-id="` + value.mediatable_id + `"
            data-media-collection-name="` + value.collection_name + `"
          >`;
        li = li + '<div class="media__infos"><p class="media__title">' + value.name + '</p>';
        li = li + '<div class="media__actions">';
        li = li + `
          <a href=""
            class="link link--edit"
            data-toggle="modal"
            data-target="#modal-media-edit"
            data-media-collection-name="`+ media_type +`"
            data-media-table-type="` + value.model_type +`"
            data-media-id="` + value.id + `"
            data-media-description="` + value.description + `"
            data-mime-type="` + value.mime_type + `"
            data-media-alt="` + value.name + `"
            data-media-name="` + value.file_name + `">
            {{ __('admin.edit') }}
          </a>`;
        li = li + '<span> / </span>';
        li = li + '{!! Form::open(array('route' => array('admin.medias.quickdestroy'))) !!}';
        li = li + '<input name="_method" value="DELETE" type="hidden">';
        li = li + '<input name="collection_name" value="'+ value.collection_name +'" type="hidden">';
        li = li + '<input name="media_id" value="'+ value.id +'" type="hidden">';
        li = li + '<a href="#" class="link link--delete" data-media-collection-name="'+ media_type +'">{{ __('admin.delete') }}</a>';
        li = li + '</form>';
        li = li + '</div>';
        li = li + '</div>';
        //media preview
        if(value.mime_type.includes("image", 0)){
          li = li + '<div class="media__preview" style="background-image:url(\'/imagecache/thumb/' + value.file_name + '\')"></div>';
        }else if(value.mime_type.includes("pdf", 0)){
          li = li + '<div class="media__preview txt"><span>PDF</span></div>';
        }else if(value.mime_type.includes("video", 0)){
          li = li + '<div class="media__preview txt"><span>VIDEO</span></div>';
        }else{
          li = li + '<div class="media__preview txt"><span>FILE</span></div>';
        }
        li = li + '</li>';
      }
    });
    // Print html
    ul.html(li);
  }


  // ----- Slugify model name ----- //
  function slugifyModel(str) {
    str = str.replace('App\\', '').toLowerCase();
    return str;
  }

  // ----- Declare modal ----- //
  $('.media-list').on('click', 'a[data-toggle="modal"]', function(e) {
    e.preventDefault();
    var button            = $(e.currentTarget);
    var article_id        = button.data('article_id');
    var column_name       = button.data('column-name');
    var media_id          = button.data('media-id');
    var media_alt         = button.data('media-alt');
    var media_name        = button.data('media-name');
    var media_description = button.data('media-description');
    var media_mime_type   = button.data('mime-type');
    var media_type        = button.data('media-type');
    var modal             = $('#modal-media-edit');
    var pic_container     = modal.find('#pic');
    var file_container    = modal.find('#file');
    var vid_container     = modal.find('#vid');
    var vid_source        = modal.find('#vid > source');
    modal.find('#input_media_id').val(media_id);
    modal.find('#input_media_alt').val(media_alt);
    modal.find('#input_media_description').val(media_description);
    pic_container.hide();
    vid_container.hide();
    file_container.hide();

    if(media_mime_type.includes("image", 0)){
      pic_container.show();
      pic_container.attr('src', '/imagecache/large/' + media_name);
    }else if(media_mime_type.includes("video", 0)){
      vid_container.show();
      vid_source.attr('src', '/storage/medias/' + media_name);
      vid_container.load();
    }else{
      file_container.show();
      file_container.attr('href', '/storage/medias/' + media_name);
      file_container.text('View file');
    }
    $("#modalButton").off('click');
  });

</script>
@endsection
