<div class="modal fade" id="modal-media-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="panel panel-default panel-edit panel-edit--single panel-settings">
        {!! Form::model($article,
        ['route' => ['admin.medias.ajaxupdate', $article->getTable()],
        'method' => 'post',
        'class' => 'form-horizontal media-edit-form',
        'name' => 'media-edit-form',
        'enctype' => 'multipart/form-data']) !!}
        {!! Form::hidden('media_id', '', ['id' => 'input_media_id'])!!}
        {!! Form::hidden('article_id', $article->id)!!}
        <div class="panel-heading">
          <div class="edit__header">
            <h1 class="edit__title">Edit media</h1>
          </div>
        </div>
        <div class="panel-body" id="modal-body">
          <div class="file">
            <img src="" id="pic">
            <a href="" id="file" target="_blank"></a>
            <video controls id="vid">
              <source src="" type="video/mp4">
            </video>
          </div>
          <div class="form-group">
            <label for="alt">Name</label>
            <input class="form-control" name="name" type="text" value="" id="input_media_alt">
          </div>
          <div class="form-group">
            <?php // TODO: TRanslate Media fields (cf Nestwork) ?>
            <label for="description">Description</label>
            <textarea class="form-control" placeholder="Intro" rows="5" name="description" id="input_media_description"></textarea>
          </div>
          <div class="form-group">
            {{-- [2] = parent_id --}}
            {{-- {!! Form::select('taxonomies[2][]', $article->taxonomiesDropdown(2), $article->tags, ['class' => 'form-control select2', 'multiple', 'style' => 'width:100%']) !!} --}}
          </div>
          <div class="form-group">
            {{-- [1] = parent_id --}}
            {{-- {!! Form::select('taxonomies[1][]', $article->taxonomiesDropdown(1,1), $article->category, ['class' => 'form-control select2', 'style' => 'width:100%', 'id' => '']) !!} --}}
          </div>
        </div>
        <div class="submit panel-action btn-container">
          <button type="button" class="btn btn-primary media-edit-save" data-dismiss="modal">{{__('admin.save')}}</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('admin.cancel') }}</button>
        </div>
        {{ Form::close() }}
      </div>
    </div>
  </div>
</div>
