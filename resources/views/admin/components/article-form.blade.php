{!! Form::hidden('id', (!empty($article->id))? $article->id : '') !!}
{!! Form::hidden('parent_id', (!empty($article->parent_id) ? $article->parent_id : 0)) !!}
{!! Form::hidden('order', (!empty($article->order))? $article->order : 0) !!}

@foreach ($errors->all() as $error)
    <span class="help-block">{{ $error }}</span>
@endforeach

    {{-- tabs --}}
    <div class="panel-heading">
      <ul class="tab-select">
      @if(count(config('translatable.locales')) > 1 )
        @foreach (config('translatable.locales') as $lang)<li role="presentation" data-tab="{{$lang}}"@if(App::getLocale() == $lang) class="active" @endif>
          <a href="#" role="tab">{{$lang}}</a>
        </li>@endforeach
      @endif
      </ul>
      <div class="edit-details">
        @if(isset($article->id))
        <div class="created_at">
          {!! Form::text('created_at', $article->created_at->format('d.m.Y'), ['class' => 'form-control']) !!}
        </div>
        @endif
        {{-- Is featured ? --}}
        <div class="is-published">
            <label>{!! Form::checkbox('featured', 1, null) !!}<span>Homepage</span></label>
        </div>
        {{-- Is published ? --}}
        <div class="is-published">
            <label>{!! Form::checkbox('published', 1, null) !!}<span>{{__('admin.published')}}</span></label>
        </div>
      </div>
    </div>
    <div class="panel-body">

        {{-- Title --}}
        <div class="form-group {!! $errors->has('title') ? 'has-error' : '' !!}">
          <label for="title">Title</label>
          {!! Form::text('title', (!empty($article->id) && !empty($article->title))? $article->title : old('title'), ['class' => 'form-control', 'placeholder' => 'Title']) !!}
          <span class="slug">
            @if(isset($article->slug))
            <i class="fa fa-link "></i>&nbsp;{{ (!empty($article->id) && !empty( $article->slug))? $article->slug : '' }}
            @endif
          </span>
        </div>
        {{-- Intro --}}
        <div class="form-group {!! $errors->has('intro') ? 'has-error' : '' !!}">
          <label for="intro">Intro</label>
          {!! Form::textarea('intro', (!empty($article->id) && !empty($article->intro))? $article->intro : old('intro'), ['class' => 'form-control', 'placeholder' => 'Introduction', 'rows' => '5']) !!}
        </div>
        {{-- Text --}}
        <div class="form-group {!! $errors->has('text') ? 'has-error' : '' !!}">
          <label for="text">Text</label>
          {!! Form::textarea('text', (!empty($article->id) && !empty($article->text))? $article->text : old('text'), ['class' => 'form-control md-editor', 'id' => 'editor', 'placeholder' => 'Use H3 (###) for question and code(</>) for author']) !!}
        </div>
    </div>
    <div class="panel-body">
      <div class="form-group {!! $errors->has('title') ? 'has-error' : '' !!}">
        <label for="author">Author</label>
        {!! Form::text('author', (!empty($article->id) && !empty($article->author))? $article->author : old('author'), ['class' => 'form-control', 'placeholder' => 'Interview by ...']) !!}
      </div>
  {{-- Taxonomy: tags --}}
  {{-- <div class="form-group"> --}}
    {{-- <label for="tags_general">Tags</label> --}}
    {{-- [2] = parent_id --}}
    {{-- {!! Form::select('taxonomies[2][]', $article->taxonomiesDropdown(2), $article->tags, ['class' => 'form-control select2', 'multiple', 'style' => 'width:100%']) !!} --}}
  {{-- </div> --}}

  {{-- Taxonomy: category --}}
  {{-- <div class="form-group"> --}}
    {{-- <label for="category">Category</label> --}}
    {{-- [1] = parent_id --}}
    {{-- {!! Form::select('taxonomies[1][]', $article->taxonomiesDropdown(1,1), $article->categories, ['class' => 'form-control select2', 'id' => '']) !!} --}}
  {{-- </div> --}}
</div>
@include('admin.components.form-submit')
