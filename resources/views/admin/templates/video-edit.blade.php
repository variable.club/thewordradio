@extends('admin.app')

@section('page_title', $data['page_title'])
@section('page_class', $data['page_class'])

@section('content')
  @if(isset($article->id))
    <div class="edit__header">
      <h1 class="edit__title">Edit</h1>
    </div>
    {!! Form::model($article, ['route' => ['admin.videos.update', $article->id], 'method' => 'put', 'class' => 'panel panel-edit main-form', 'id' => 'main-form', 'files' => true]) !!}
  @else
    <div class="edit__header">
      <h1 class="edit__title">Create</h1>
    </div>
    {!! Form::model($article, ['route' => ['admin.videos.store'], 'method' => 'post', 'class' => 'panel panel-edit main-form', 'id' => 'main-form', 'files' => true]) !!}
  @endif
  <div id="validation"></div>
  {!! Form::hidden('id', (!empty($article->id))? $article->id : '') !!}
  {!! Form::hidden('parent_id', (!empty($article->parent) ? $article->parent->id : 0)) !!}
  {!! Form::hidden('order', (!empty($article->order))? $article->order : 0) !!}

  @foreach ($errors->all() as $error)
    <span class="help-block">{{ $error }}</span>
  @endforeach

  <div class="panel-heading">
    <div class=""></div>
    <div class="edit-details">
      @if(isset($article->id))
      <div class="created_at">
        {!! Form::text('created_at', $article->created_at->format('d.m.Y'), ['class' => 'form-control']) !!}
      </div>
      @endif
      {{-- Is featured ? --}}
      <div class="is-published">
          <label>{!! Form::checkbox('featured', 1, null) !!}<span>Homepage</span></label>
      </div>
      {{-- Is published ? --}}
      <div class="is-published">
        <label>{!! Form::checkbox('published', 1, null) !!}<span>{{__('admin.published')}}</span></label>
      </div>
    </div>
  </div>
  <div class="panel-body">
    {{-- Title --}}
    <div class="form-group {!! $errors->has('title') ? 'has-error' : '' !!}">
      <label for="title">Title</label>
      {!! Form::text('title', (!empty($article->id) && !empty($article->title)) ? $article->title : old('title'), ['class' => 'form-control', 'placeholder' => 'Title']) !!}
      <span class="slug">
        @if(isset($article->slug))
        <i class="fa fa-link "></i>&nbsp;{{ (!empty($article->id) && !empty($article->slug)) ? $article->slug : '' }}
        @endif
      </span>
    </div>
    {{-- Url --}}
    <div class="form-group {!! $errors->has('url') ? 'has-error' : '' !!}">
      <label for="url">Video url</label>
      {!! Form::text('url', (!empty($article->id) && !empty($article->title)) ? $article->url : old('url'), ['class' => 'form-control', 'placeholder' => 'Starting with http://']) !!}
    </div>
    {{-- Intro --}}
    <div class="form-group {!! $errors->has('intro') ? 'has-error' : '' !!}">
      <label for="intro">Intro</label>
      {!! Form::textarea('intro', (!empty($article->id) && !empty($article->intro)) ? $article->intro : old('intro'), ['class' => 'form-control', 'placeholder' => 'Introduction', 'rows' => '5']) !!}
    </div>
    {{-- Text --}}
    <div class="form-group {!! $errors->has('text') ? 'has-error' : '' !!}">
      <label for="text">Text</label>
      {!! Form::textarea('text', (!empty($article->id) && !empty($article->text)) ? $article->text : old('text'), ['class' => 'form-control md-editor', 'id' => '', 'placeholder' => 'Text']) !!}
    </div>
  </div>
  <div class="panel-body">
    {{-- Taxonomy: Category --}}
    @if(empty($article->parent))
    <div class="form-group">
      <label for="tags_general">Category</label>
      {{-- [2] = parent_id --}}
      {!! Form::select('taxonomies[681][]', $article->taxonomiesDropdown(681), $article->taxonomies, ['class' => 'form-control select2', 'style' => 'width:100%']) !!}
    </div>
    @endif
    {{-- Taxonomy: tags --}}
    <div class="form-group">
      <label for="tags_general">Tags</label>
      {{-- [2] = parent_id --}}
      {!! Form::select('taxonomies[1][]', $article->taxonomiesDropdown(1), $article->taxonomies, ['class' => 'form-control select2', 'multiple', 'style' => 'width:100%']) !!}
    </div>
  </div>
  @include('admin.components.form-submit')
  {!! Form::close() !!}
  <aside class="article-aside">
    @include('admin.components.media-aside')
  </aside>
  @if(isset($article->id))
    <ul class="panel-footer">
      <li>@include('admin.components.delete-form', ['model' => $article, 'model_name' => 'videos'])</li>
      <li><a href="{{ route('videos.show', [$article->slug]) }}" class="link" target="_blank">{{ __('admin.preview') }}</a> {{ __('admin.on_website') }}</li>
    </ul>
  @endif
@endsection

@section('meta')
  <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection
