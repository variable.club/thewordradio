@extends('admin.app')

@section('page_title', $data['page_title'])
@section('page_class', $data['page_class'])

@section('content')
  <div class="panel panel-default panel-edit panel-edit--single panel-settings">
    <div class="panel-heading">
      <div class="edit__header">
        <h1 class="edit__title">{{__('admin.settings')}}</h1>
      </div>
    </div>
      @foreach ($settings as $setting)
        {!! Form::model($setting, ['route' => ['admin.settings.update', $setting->id ], 'method' => 'put', 'class' => 'form-horizontal']) !!}
        <div class="panel-body">
          <div class="form-group">
            <label for="content" class="control-label">
              {{ $setting->name }}
              <span class="description">{{ $setting->description }}</span>
            </label>
            <div class="">
              <?php $form_type = $setting->name == 'Google analytics' ? 'text' :'textarea'; ?>
              {!! Form::$form_type('content', $setting->content, ['class' => 'form-control', 'rows' => '4']) !!}
            </div>
          </div>
        </div>
        {!! Form::submit(__('admin.save'), ['class' => 'btn btn-primary small', 'name' => 'finish']) !!}
        {!! Form::close() !!}
      @endforeach
  </div>
@endsection

@section('meta')
  <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection
