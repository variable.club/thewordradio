@extends('admin.app')

@section('page_title', $data['page_title'])
@section('page_class', $data['page_class'])

@section('content')
  @if(isset($article->id))
    <div class="edit__header">
      <h1 class="edit__title">Edit</h1>
    </div>
    {!! Form::model($article, ['route' => ['admin.events.update', $article->id], 'method' => 'put', 'class' => 'panel panel-edit main-form', 'id' => 'main-form', 'files' => true]) !!}
  @else
    <div class="edit__header">
      <h1 class="edit__title">Create</h1>
    </div>
    {!! Form::model($article, ['route' => ['admin.events.store'], 'method' => 'post', 'class' => 'panel panel-edit main-form', 'id' => 'main-form', 'files' => true]) !!}
  @endif
  <div id="validation"></div>
  {!! Form::hidden('id', (!empty($article->id))? $article->id : '') !!}
  {!! Form::hidden('parent_id', (!empty($article->parent_id) ? $article->parent_id : 0)) !!}
  {!! Form::hidden('order', (!empty($article->order))? $article->order : 0) !!}

  @foreach ($errors->all() as $error)
    <span class="help-block">{{ $error }}</span>
  @endforeach

  <div class="panel-heading">
    <div class=""></div>
    <div class="edit-details">
      @if(isset($article->id))
      <div class="created_at">
        {!! Form::text('created_at', $article->created_at->format('d.m.Y'), ['class' => 'form-control']) !!}
      </div>
      @endif
      @if($article->parent_id == 0)
      {{-- Is featured ? --}}
      <div class="is-published">
          <label>{!! Form::checkbox('featured', 1, null) !!}<span>Homepage</span></label>
      </div>
      @endif
      {{-- Is published ? --}}
      <div class="is-published">
        <label>{!! Form::checkbox('published', 1, null) !!}<span>{{__('admin.published')}}</span></label>
      </div>
    </div>
  </div>
  <div class="panel-body">
    {{-- Title --}}
    <div class="form-group {!! $errors->has('title') ? 'has-error' : '' !!}">
      <label for="title">Title</label>
      {!! Form::text('title', (!empty($article->id) && !empty($article->title)) ? $article->title : old('title'), ['class' => 'form-control', 'placeholder' => 'Title']) !!}
      <span class="slug">
        @if(isset($article->slug))
        <i class="fa fa-link "></i>&nbsp;{{ (!empty($article->id) && !empty($article->slug)) ? $article->slug : '' }}
        @endif
      </span>
    </div>
    {{-- start_date  --}}
    <div class="form-group {!! $errors->has('start_date') ? 'has-error' : '' !!}">
      <label for="start_date">Start date</label>
      {!! Form::date('start_date', null, ['class' => 'form-control', 'placeholder' => 'aaa-mm-jj']) !!}
    </div>
    {{-- end_date  --}}
    <div class="form-group {!! $errors->has('end_date') ? 'has-error' : '' !!}">
      <label for="end_date">End date</label>
      {!! Form::date('end_date', null, ['class' => 'form-control', 'placeholder' => 'aaa-mm-jj']) !!}
    </div>
    @if($article->parent_id > 0)
    {{-- Place --}}
    <div class="form-group {!! $errors->has('place') ? 'has-error' : '' !!}">
      <label for="title">Place</label>
      {!! Form::text('place', (!empty($article->id) && !empty($article->place)) ? $article->place : old('place'), ['class' => 'form-control', 'placeholder' => 'Adress']) !!}
    </div>
    @endif
    {{-- Intro --}}
    {{-- <div class="form-group {!! $errors->has('intro') ? 'has-error' : '' !!}">
      <label for="intro">Intro</label>
      {!! Form::textarea('intro', (!empty($article->id) && !empty($article->intro)) ? $article->intro : old('intro'), ['class' => 'form-control', 'placeholder' => 'Introduction', 'rows' => '5']) !!}
    </div> --}}
    {{-- Text --}}
    <div class="form-group {!! $errors->has('text') ? 'has-error' : '' !!}">
      <label for="text">Text</label>
      {!! Form::textarea('text', (!empty($article->id) && !empty($article->text)) ? $article->text : old('text'), ['class' => 'form-control md-editor', 'id' => '', 'placeholder' => 'Text']) !!}
    </div>
  </div>
  @if($article->parent_id)
  <div class="panel-body">
    {{-- Taxonomy: tags --}}
    <div class="form-group">
      <label for="tags_general">Category</label>
      {{-- [2] = parent_id --}}
      {!! Form::select('taxonomies[680][]', $article->taxonomiesDropdown(680, 1), $article->taxonomies, ['class' => 'form-control select2', 'style' => 'width:100%']) !!}
    </div>
  </div>
  @endif
  @include('admin.components.form-submit')
  {!! Form::close() !!}
  <aside class="article-aside">
    @include('admin.components.media-aside')
  </aside>
  @if(isset($article->id))
    <ul class="panel-footer">
      <li>@include('admin.components.delete-form', ['model' => $article, 'model_name' => 'events'])</li>
      <li><a href="{{ route('events.show', [$article->slug]) }}" class="link" target="_blank">{{ __('admin.preview') }}</a> {{ __('admin.on_website') }}</li>
    </ul>
  @endif
@endsection

@section('meta')
  <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection
