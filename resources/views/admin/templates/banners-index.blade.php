@extends('admin.app')

@section('page_title', $data['page_title'])
@section('page_class', $data['page_class'])

@section('content')
<div class="panel panel-default">
  <div class="table-responsive">
    @include('admin.components.datatable-loading')
    <a href="{{ route('admin.banners.create') }}" class="btn btn-primary btn-xs">{{__('admin.add')}}</a>
    <table class="panel-body table table-hover table-bordered table-striped table-reorderable" id="datatable" style="width:100%">
      <?php // TODO: THEAD caché, à afficher selon les besoins + ajouter flèches si tri par colonne ?>
        <thead class="hidden">
          <tr>
            <th class="is-published"></th>
            <th class="main-column">Title</th>
            <th class="hidden-small">Dates</th>
            <th></th>
          </tr>
        </thead>
    </table>
  </div>
</div>
@endsection

@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/rowreorder/1.2.3/js/dataTables.rowReorder.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    var table = $('#datatable').DataTable({
      responsive: true,
      autoWidth: false,
      processing: true,
      serverSide: true,
      iDisplayLength: 50,
      stateSave: true,
      rowReorder: {
          selector: '.reorder'
      },
      colReorder: false,
      dom       : '<"panel-heading"f> <"panel-body"t> <"panel-footer"<li>p>',
      initComplete: function(settings, json) {
          $('div.datatable-loading').hide();
        },
        ajax: '{{ route('admin.' .$data['table_type']. '.getdata') }}',
      language: {
        'search': '',
        'searchPlaceholder': '{{ $data['page_title'] }}',
        'paginate': {
          'previous': '&larr;',
          'next': '&rarr;'
        },
        'lengthMenu': "{{__('admin.datatable_lengthMenu')}}",
        'zeroRecords': "{{__('admin.datatable_zeroRecords')}}",
        'info': "{{__('admin.datatable_info')}}",
      },
      columns: [
        //IDEA: Ajouter column de contenu cachée pour recherche (texte d'article ect)?
        {data: 'order', render: function ( data, type, row, meta ) {
          if(row.published == 1){
            return '<i class="fa fa-circle icon-published" title="{{__('admin.published')}}"></i>';
          }else {
            return '<i class="fa fa-circle-o icon-published" title="{{__('admin.not_published')}}"></i>';
          }
        }, name: 'order', searchable: false, orderable: false, class: 'is-published'},
        {data: 'title_link', render: function ( data, type, row, meta ) {
          return data;
        }, name: 'title', orderable: false, class: 'main-column'},
        {data: 'dates', render: function ( data, type, row, meta ) {
          return '<div class="text-content">'+ data + '</div>';
        }, name: 'dates', searchable: true, orderable: false, class: 'hidden-small'},
        {data: 'action', render: function ( data, type, row, meta ) {
          return data + '&nbsp; &nbsp;';
        }, name: 'action', orderable: false, searchable: false, class:'faded'}
      ]
    });

    table.on( 'row-reorder', function ( e, diff, edit ) {
      var articlesArray = [];
      for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
        var rowData = table.row( diff[i].node ).data();
        var newOrder = diff[i].newPosition;
        articlesArray.push({
          id: rowData.id,
          position: newOrder
        });
      }
      var jsonString = JSON.stringify(articlesArray);
      $.ajax({
        url     : '{{ route('admin.reorder', ['table_type' => $data['table_type']]) }}',
        type    : 'POST',
        data    : jsonString,
        dataType: 'json',
        success : function ( json ) {
          $('#datatable').DataTable().ajax.reload(); // refresh datatable
            $.each(json, function (key, msg) {
        	  // handle json response
          });
        }
      });
    });
    $.fn.dataTable.ext.errMode = 'throw';
});
</script>
@endsection
