@component('mail::message')
# Nouvel utilisateur
Nom d'utilisateur : {{ $user->email }} <br />


@component('mail::button', ['url' => $url])
Se connecter
@endcomponent

Merci !
@endcomponent
