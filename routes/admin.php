<?php
// Emails tests
// Route::get('mailable', function () {
//     $user = App\User::find(1);
//     return new App\Mail\AdminNewUser($user);
// });
// Admin specific routes
Route::get('/', ['as' => 'root', 'uses' => 'ShowsController@index']);
Route::get('users/getdata', 'UsersController@getDataTable')->name('users.getdata');
Route::resource('users', 'UsersController');
Route::get('roles/getdata', 'RoleController@getDataTable')->name('roles.getdata');
Route::resource('roles', 'RoleController');
Route::get('permissions/getdata', 'PermissionController@getDataTable')->name('permissions.getdata');
Route::resource('permissions', 'PermissionController');
Route::resource('roles', 'RoleController');
// Shows
Route::get('shows/getdata', 'ShowsController@getDataTable')->name('shows.getdata');
Route::resource('shows', 'ShowsController');
// Residents
Route::get('residents/getdata', 'ResidentsController@getDataTable')->name('residents.getdata');
Route::resource('residents', 'ResidentsController');
//settings
Route::resource('settings', 'SettingsController');
// Taxonomies
Route::get('taxonomies/getdata', 'TaxonomiesController@getDataTable')->name('taxonomies.getdata');
Route::resource('taxonomies', 'TaxonomiesController', ['except' => ['create']]);
Route::get('taxonomies/create/{parent_id}', 'TaxonomiesController@create')->name('taxonomies.create');
Route::post('taxonomies/reorder/{id}', 'TaxonomiesController@reorder')->name('taxonomies.reorder');
Route::post('{table_type}/reorder', 'AdminController@orderObject')->name('reorder');
// Medias
Route::get('medias/getdata', 'MediasController@getDataTable')->name('medias.getdata');
Route::delete('medias/quickdestroy', 'MediasController@quickDestroy')->name('medias.quickdestroy');
Route::resource('medias', 'MediasController');
Route::get('mediasArticle/{model_type}/{model_id}/{collection_name}', 'MediasController@mediasArticle')->name('medias.article');
Route::post('medias/storeandlink/{mediatable_type}/{article_id?}', 'MediasController@storeAndLink')->name('medias.storeandlink');
Route::post('medias/reorder/{model_type}/{article_id}/{collection_name}', 'MediasController@reorder')->name('medias.reorder');
// Route::post('medias/get', 'MediasController@getFromArray');
Route::post('medias/ajaxUpdate/{mediatable_type}', 'MediasController@AjaxUpdate')->name('medias.ajaxupdate');
Route::post('fileupload', 'MediasController@fileUpload')->name('fileupload');
// Events
Route::get('events/{parent_id}/getdata', 'EventsController@getDataTable')->name('events.getdata');
Route::resource('events', 'EventsController', ['except' => ['create', 'index']]);
Route::get('events/{parent_id}/create', 'EventsController@create')->name('events.create');
Route::get('events/{parent_id?}/index', 'EventsController@index')->name('events.index');
Route::post('events/reorder/{id}', 'EventsController@reorder')->name('events.reorder');
// Articles
Route::get('articles/getdata', 'ArticlesController@getDataTable')->name('articles.getdata');
Route::resource('articles', 'ArticlesController');
// Videos
Route::get('videos/{parent_id}/getdata', 'VideosController@getDataTable')->name('videos.getdata');
Route::resource('videos', 'VideosController', ['except' => ['create', 'index']]);
Route::get('videos/{parent_id}/create', 'VideosController@create')->name('videos.create');
Route::get('videos/{parent_id?}/index', 'VideosController@index')->name('videos.index');
Route::post('videos/reorder/{id}', 'VideosController@reorder')->name('videos.reorder');
// Banners
Route::get('banners/getdata', 'BannersController@getDataTable')->name('banners.getdata');
Route::resource('banners', 'BannersController');
// Pages
Route::get('pages/{parent_id}/getdata', 'PagesController@getDataTable')->name('pages.getdata');
Route::resource('pages', 'PagesController', ['except' => ['create', 'index']]);
Route::get('pages/{parent_id}/create', 'PagesController@create')->name('pages.create');
Route::get('pages/{parent_id?}/index', 'PagesController@index')->name('pages.index');
Route::post('pages/reorder/{id}', 'PagesController@reorder')->name('pages.reorder');
// Datatables
Route::get('datatable', 'DataTablesController@datatable');
Route::get('datatable/getArticles', 'DataTablesController@getArticles')->name('datatable/getdata');
