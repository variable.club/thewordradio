<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth
Auth::routes();

// Language switcher
Route::get('lang/{language}', ['as' => 'lang.switch', 'uses' => 'LocaleController@switch']);
// Homepage
Route::get('/', 'HomeController@index')->name('homepage');
// Pages index
Route::get('/pages/{article_slug}', 'PagesController@show')->name('pages.show');
Route::get('/about', 'PagesController@about')->name('pages.about');

// Article
Route::get('/interviews', 'ArticlesController@index')->name('articles.index');
Route::get('/interviews/{slug}', 'ArticlesController@show')->name('articles.show');
// Residents
Route::get('/residents', 'ResidentsController@index')->defaults('category', 'the-word')->name('residents.index');
Route::get('/residents/category/{slug}', 'ResidentsController@index')->name('residents.indexbycategory');
Route::get('/residents/{slug}', 'ResidentsController@show')->name('residents.show');
// Shows
Route::get('/shows', 'ShowsController@index')->name('shows.index');
Route::get('/shows/tag/{slug}', 'ShowsController@indexByTag')->name('shows.indexbytags');
Route::get('/residents/{resident_slug}/shows/{slug}', 'ShowsController@show')->name('shows.show');
// Events
Route::get('/agenda', 'EventsController@index')->name('events.index');
Route::get('/agenda/{slug}', 'EventsController@show')->name('events.show');
Route::get('/agenda/single/{slug}', 'EventsController@showSingle')->name('events.single.show');
// Videos
Route::get('/videos/category/{slug}/{parent?}', 'VideosController@index')->name('videos.index');
Route::get('/videos/{slug}', 'VideosController@show')->name('videos.show');
// Search
Route::get('/search', 'HomeController@index')->name('search');
// Tags
Route::get('/tags/{slug}', 'HomeController@index')->name('tags');
// Schedule
Route::get('/schedule', 'HomeController@schedule')->name('schedule');
// Search
Route::get('/search', 'SearchController@search')->name('search');

// Redirects V1
Route::redirect('/series/{resident_slug}/shows/{slug}', '/residents/{resident_slug}/shows/{slug}');
Route::redirect('/series/{resident_slug}', '/residents/{resident_slug}');
